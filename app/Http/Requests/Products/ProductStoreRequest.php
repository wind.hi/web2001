<?php

namespace App\Http\Requests\Products;

use App\Http\Requests\BaseRequest;

class ProductStoreRequest extends BaseRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:products,name',
            'cate_id' => 'required|exists:categories,id',
            'manufacturer_id' => 'required|exists:manufacturers,id',
            'origin_id' => 'required|exists:origins,id',
            'intro' => 'nullable',
            'short_des' => 'nullable',
            'body' => 'nullable',
            'base_price' => 'nullable|integer',
            'price' => 'required|integer',
            'status' =>'required|in:0,1',
            'image' => 'required|file|mimes:jpg,jpeg,png|max:3000',
            'galleries' => 'required|array|min:1|max:20',
            'galleries.*.image' => 'required|file|mimes:png,jpg,jpeg|max:10000',

            'attributes' => 'nullable|array',
        ];

        $attributeInput = $this->get('attributes');

        if(($attributeInput)) {
            foreach ($attributeInput as $key => $attribute) {
                $rules['attributes.'.$key.'.'.'attribute_id']   = 'required';
                $rules['attributes.'.$key.'.'.'value']   = 'required';
            }
        }

        return $rules;
    }

}
