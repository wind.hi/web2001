<?php

namespace App\Http\Requests\Products;

use App\Http\Requests\BaseRequest;

class ProductUpdateRequest extends BaseRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =[
            'name' => 'required|unique:products,name,'.$this->route('id').",id",
            'cate_id' => 'required|exists:categories,id',
            'manufacturer_id' => 'required|exists:manufacturers,id',
            'origin_id' => 'required|exists:origins,id',
            'short_des' => 'nullable',
            'intro' => 'nullable',
            'body' => 'nullable',
            'base_price' => 'nullable|integer',
            'price' => 'required|integer',
            'status' =>'required|in:0,1',
            'image' => 'nullable|file|mimes:jpg,jpeg,png|max:3000',
            'galleries' => 'required|array|min:1|max:20',
            'galleries.*.image' => 'required_without:galleries.*.id|file|mimes:png,jpg,jpeg',

            'attributes' => 'nullable|array',

        ];

        $attributeInput = $this->get('attributes');

        if(($attributeInput)) {
            foreach ($attributeInput as $key => $attribute) {
                $rules['attributes.'.$key.'.'.'attribute_id']   = 'required';
                $rules['attributes.'.$key.'.'.'value']   = 'required';
            }
        }

        return $rules;
    }

}
