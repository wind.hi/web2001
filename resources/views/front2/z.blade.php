
<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
<head><title>
        Bếp gas sự lựa chọn thông minh của mọi gia đình!
    </title><meta name="description" content="Bếp gas là một vật dụng đã trở nên quá quen thuộc trong mọi căn bếp Việt. Hiện nay, bếp gas không chỉ có mỗi bếp gas dương cổ điển mà còn có bếp gas âm, bếp gas hồng ngoại, bếp gas dương hồng ngoại,...Bếp gas nói chung đều có thiết kế hiện đại, mẫu mã đa dạng, tính thẩm mỹ cao và đặc biệt có độ an toàn cao" /><meta name="keywords" content="Bếp gas" /><link rel="canonical" href="https://beptot.vn/bep-gas.html" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" /><meta itemprop="description" /><meta itemprop="image" />

    <!-- Open Graph data -->
    <meta property="og:title" content="Bếp gas sự lựa chọn thông minh của mọi gia đình!" /><meta property="og:url" content="https://beptot.vn/bep-gas.html" /><meta property="og:image" content="https://beptot.vn/Data/ResizeImage/files/icon/bepgassx200x200x5.webp" /><meta property="og:description" content="Bếp gas là một vật dụng đã trở nên quá quen thuộc trong mọi căn bếp Việt. Hiện nay, bếp gas không chỉ có mỗi bếp gas dương cổ điển mà còn có bếp gas âm, bếp gas hồng ngoại, bếp gas dương hồng ngoại,...Bếp gas nói chung đều có thiết kế hiện đại, mẫu mã đa dạng, tính thẩm mỹ cao và đặc biệt có độ an toàn cao" /><meta property="og:site_name" content="beptot.vn" /><meta property="fb:app_id" content="2075494006093101" /><meta property="og:type" name="og_type" content="article" /><meta property="og:locale:alternate" content="vi_VN" /><meta property="article:author" content="https://www.facebook.com/beptot.vn" /><meta property="article:publisher" content="https://www.facebook.com/beptot.vn" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /><meta http-equiv="content-type" content="text/html; charset=utf-8" /><meta name="author" content="beptot.vn" /><meta name="copyright" content="Copyright © 2017 beptot.vn" /><meta name="generator" content="beptot.vn" /><meta name="language" content="vietnamese" /><meta name="geo.region" content="Vietnamese" /><meta name="revisit-after" content="1 days" /><meta name="robots" content="index,follow" /><link href="/Content/pc/images/favicon.png" rel="shortcut icon" type="image/x-icon" />

    <link type="text/css" href="/Content/desktop/css/bootstrap.min.css?v=637742176178019765" rel="stylesheet" />
    <link type="text/css" href="/Content/desktop/css/owl.carousel.css?v=637744756545641942" rel="stylesheet" />
    <link type="text/css" href="/Content/desktop/css/allpage.css?v=637748409762603265" rel="stylesheet" />
    <style >@charset "utf-8";@import url("/Content/desktop/css/font-awesome.css");@import url("/Content/desktop/css/font-akr.css");html{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}*{margin:0;padding:0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}body{background:#f5f5f5;color:#222;font-size:14px;font-family:Arial,Helvetica,sans-serif}img{width:auto;height:auto;max-width:100%}:focus{outline:0 !important}figure{margin:0;padding:0}img[src*=".svg"]{height:100%}iframe{border:none}p{margin-bottom:10px}a{color:#288ad6}a:hover,a:focus{text-decoration:none;color:#0074c1}ul,ol{margin:0}h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{color:inherit;font-family:inherit;font-weight:600;margin:0}h1{font-size:39px}h2{font-size:32px}h3{font-size:26px}h4{font-size:19px}h5{font-size:15px}h6{font-size:13px}ol{position:relative}ul{list-style:none}.fl{float:left}.fr{float:right}.lazy{background:#fff url("/Content/desktop/css/images/loading.gif") center center no-repeat}.overlay{z-index:3;position:fixed;width:100%;height:100%;left:0;top:0;transition:0.2s;background:rgba(0,0,0,.6);visibility:hidden;opacity:0}.container{width:1300px;min-width:1300px}.overlay-visible{visibility:visible;opacity:1}.hm-reponsive{position:relative;height:auto !important;width:100%;padding-top:56.25%;overflow:hidden}.hm-reponsive img{object-fit:cover;margin:auto;position:absolute;width:100%;height:100%;top:0;left:0}.reponsive-img{position:relative;height:auto !important;width:100%;padding-top:56.25%;overflow:hidden}.reponsive-img img{margin:auto;position:absolute;max-width:100%;max-height:100%;top:0;left:0;right:0;bottom:0}.er-banner-top{background-color:#fbf6ed;width:100%;overflow:hidden;text-align:center;position:relative}.er-banner-top .close_top_banner{position:absolute;color:#fff;right:20px;top:50%;cursor:pointer;margin-top:-10px;background:rgba(220,0,33,.8);border-radius:50%;padding:2px;width:20px;height:20px;opacity:.8}.alink-all{position:absolute;top:0;left:0;height:100%;width:100%;z-index:10}.back-to-top{position:fixed;bottom:132px;right:-50px;transition:all .5s ease-out;-webkit-transition:all .5s ease;-moz-transition:all .5s ease;-o-transition:all .5s ease;width:36px;height:36px;border-radius:50%;line-height:39px;background:rgba(0,0,0,.3);box-shadow:0 7px 10px 0 rgba(106,65,65,.21);text-align:center;display:inline;z-index:999999;cursor:pointer}.back-to-top a{display:block;color:#fff}.back-to-top:hover{background:#e00;color:#fff}.back-to-top:hover a,.back-to-top a:focus{color:#fff}.back-to-top.display{bottom:132px;right:20px}label{display:inline-block;max-width:100%;margin-bottom:5px}.header-wrap{width:100%;background:#f34723;margin:auto;z-index:12;position:relative;display:block;border-bottom:1px solid #f5f5f5}.logo{float:left;width:20%;text-align:center;padding:3px 0 2px 0;position:relative}.logo img{}.content-main{clear:both}.headsearch{float:left;width:33%;box-shadow:rgba(98,98,98,0.5) 0 1px 2px 0;height:36px;margin:9px 0 0 0;background:#fff;position:relative;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px}.headsearch .topinput{display:block;width:100%;height:36px;border:0;position:relative;padding:6px 12px;font-size:14px;line-height:1.42857143;color:#555;background-color:#fff;background-image:none;border-radius:4px}.nav-user-txt{float:right;margin-top:15px;white-space:nowrap;background:transparent}.nav-user-txt .user-avatar{height:30px;width:30px;border-radius:50%;text-align:center}.nav-user-txt li{list-style:none;position:relative;float:left;margin:0;padding:0 0 0 10px}.nav-user-txt li.user{height:42px}.btnviewed{background:gold;border-radius:4px;margin-top:-6px;padding:9px 28px 9px 5px !important;color:#fff;font-size:12px !Important;cursor:pointer}.btnviewed span{position:relative}.btnviewed span:before{content:'';width:0;height:0;border-top:6px solid #fff;border-left:6px solid transparent;border-right:6px solid transparent;position:absolute;top:6px;right:-18px}.product-recently .empty{text-align:center !Important;margin-top:32px !Important;color:#333 !Important;font-size:13px !Important;line-height:16px}.product-recently .recently{padding:15px 0}.product-recently .recently .item{text-align:center}.product-recently .recently a{white-space:normal;display:block}.product-recently .recently img{display:inline !Important;height:45px;width:auto}.product-recently .recently h3{font-size:13px;height:31px;font-weight:normal;overflow:hidden;text-align:center;text-overflow:ellipsis;display:-webkit-box;-webkit-line-clamp:2;color:#333;margin:5px 0;-webkit-box-orient:vertical;-webkit-box-pack:center}.product-recently .recently strong{font-size:12px;display:block}.product-recently .recently strong.red{color:#d0021b}.product-recently .recently strong.oldprice{text-decoration:line-through;font-weight:normal;color:#333;display:inline-block}.nav-user-txt li .icon{position:relative;height:24px;width:24px;display:table-cell;vertical-align:middle}.nav-user-txt li .icon i{margin:0 auto;font-size:24px;color:white}.nav-user-txt li p{color:black;line-height:12px;font-size:16px;font-weight:500;padding:0 0 0 5px;display:table-cell;vertical-align:middle}.nav-user-txt li p span:nth-child(1){font-size:18px;font-weight:500;color:#fff;display:block;margin:0 0 3px 0;font-weight:bold;line-height:25px}.nav-user-txt li p span:nth-child(2){opacity:1;color:#fff;font-size:12px}.nav-user-txt li .dropdown-content{position:absolute;top:41px;left:calc(50% + 10px);transform:translateX(-50%);width:200px;box-shadow:0 0 8px rgba(0,0,0,0.2);background-color:#fff;z-index:2;border-radius:5px;opacity:0;visibility:hidden;transition:opacity 0.2s}.nav-user-txt li.sp-view{position:static !Important;height:41px}.nav-user-txt li .dropdown-content.dropdown-view{position:absolute;border-radius:0 0 6px 6px;margin:auto;width:1200px;height:148px;top:56px;left:0 !Important;transform:translateX(0%);right:0 !Important;box-shadow:0 0 8px rgba(0,0,0,0.2);z-index:18;opacity:0}.nav-user-txt li .dropdown-content .pd-user{padding:15px 12px 20px 12px}.nav-user-txt li .icon span.mount{height:18px;width:18px;padding:0;color:#000;font-size:12px;border-radius:50%;background-color:#ffe300;display:block;line-height:18px;text-align:center;position:absolute;top:-6px;right:-6px}.nav-user-txt li .dropdown-content p{display:block;padding:10px 0;margin:0;text-align:left;padding-left:15px;line-height:24px;border-bottom:1px solid #f0f0f0}.nav-user-txt li .dropdown-content p:last-child{border:none}.nav-user-txt li .dropdown-content p span:nth-child(1){color:#39393a;font-size:14px;padding:0}.nav-user-txt li .dropdown-content p span:nth-child(2){font-size:24px}.nav-user-txt li .dropdown-content p a span{font-size:24px !important;font-weight:500 !important}.nav-user-txt li .dropdown-content p a span.color2{color:#6ece1a}.nav-user-txt li .dropdown-content p a span.color1{color:#e73435}.nav-user-txt li .dropdown-content p span:nth-child(2).color3{color:#39393a;font-weight:normal}.nav-user-txt li .dropdown-content:before{content:"";position:absolute;top:-20px;left:46%;border:10px solid transparent;border-bottom:10px solid #fff}.nav-user-txt li .dropdown-content.dropdown-view:before{left:61%}.nav-user-txt .dropdown-content .level a{margin-left:24px;text-decoration:underline;color:#ed0017}.nav-user-txt li:hover .dropdown-content{opacity:1;visibility:visible}.nav-user-txt .dropdown-content .pd-user .icon_pic{display:inline-block;height:45px;width:45px;border-radius:50%;margin-right:10px;text-align:center}.nav-user-txt .dropdown-content > div > div > span{display:inline-block;vertical-align:top;max-width:70%}.nav-user-txt .dropdown-content .pd-user .nameSpan{display:block;margin-bottom:3px;font-size:16px;color:#333;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.nav-user-txt .dropdown-content .level{display:inline-block;color:#999}.nav-user-txt .dropdown-content .info{display:flex;padding:8px;border-radius:3px;background-color:#f3f3f3;line-height:26px;overflow:hidden;text-align:center}.nav-user-txt .dropdown-content .info li{flex:1}.nav-user-txt .dropdown-content .info a{display:block;font-size:12px;color:#999}.nav-user-txt .dropdown-content .info a span{font-weight:bold;font-size:16px;color:#333}.nav-user-txt .dropdown-content .func-user{display:flex;flex-wrap:wrap;margin:5px 0;justify-content:center;color:#666}.nav-user-txt .dropdown-content .func-user a{box-sizing:border-box;width:100%;color:#333;line-height:34px;display:inline-block;font-size:14px}.nav-user-txt .dropdown-content .func-user a i{vertical-align:middle;margin-right:5px;font-size:20px;font-weight:400 !important}.nav-user-txt .dropdown-content a.nameLogout{display:block;max-width:220px;height:34px;margin:0 auto;background:linear-gradient(-180deg,#fbfbfb 0,#f1f1f1 100%);border:1px solid #c3c3c3;box-shadow:0 1px 3px 0 rgba(0,0,0,.12);line-height:34px;font-size:14px;color:#333;border-radius:4px;text-align:center;cursor:pointer}.nav-user-txt .dropdown-content a.nameLogout:hover{background:#f00;border:1px solid #e00;color:#fff}.nav-user-txt li a{font-size:14px;display:flex;color:#fff}.nav-user-txt li a span{padding-left:8px;display:flex;font-size:13px;align-items:center}.nav-user-txt li a span.user{white-space:nowrap;text-overflow:ellipsis;width:80px;overflow:hidden}.nav-user-txt .select{padding:4px 16px;color:#fff;border-radius:20px;background:#ea0303}.headsearch .btntop{width:40px;height:36px;border:0;cursor:pointer;position:absolute;right:0;top:0;font-size:16px;background:#fff;color:#888;font-weight:bold;border-radius:0 3px 3px 0;transition:all 0.3s ease-in-out}#cart-box{width:auto;float:right;height:38px;padding:23px 6px 0 28px;position:relative}#cart-box i{font-size:25px;color:#f00}#cart-box .cart span{width:18px;height:18px;background:#ffff00;text-align:center;font-size:12px;color:#000;border-radius:20px;position:absolute;top:15px;right:-2px;line-height:18px;font-weight:bold}.switchboard{float:right;overflow:hidden;font-size:13px;color:#fff;font-weight:600;padding:9px 0 7px 15px}.switchboard span{display:block;overflow:hidden;font-size:12px;font-weight:300;color:#fff;padding-top:2px}header .supermarket{float:right;overflow:visible;font-size:15px;color:#fff;font-weight:600;padding:7px 0 7px 26px;position:relative}header .supermarket:before,header .supermarket:after{content:'';width:0;height:0;border-left:6px solid #fff;border-top:6px solid transparent;border-bottom:6px solid transparent;position:absolute;top:11px;right:-10px}header .supermarket span{display:block;overflow:hidden;font-size:12px;font-weight:300;color:yellow;padding-top:3px}header .promotion{float:right;overflow:hidden;font-size:15px;color:#f16233;font-weight:600;padding:7px 0 7px 24px}header .promotion span{display:block;overflow:hidden;font-size:12px;color:#4a90e2;padding-top:3px}header .supermarket:after{margin:2px 2px 0 0;border-left-color:#4a90e2;border-width:4px}nav.menu_main_cate{display:block;background:#f34723;box-shadow:2px 2px 3px rgba(0,0,0,.1)}nav.menu_main_cate ul.menu-right-head{text-align:center}nav.menu_main_cate ul.menu-right-head > li{text-align:left;padding:10px 0;display:inline-block}nav.menu_main_cate ul.menu-right-head > li > a{padding:0 7px;font-size:13px;color:#fff;text-transform:none;font-weight:500;display:flex;align-items:center}nav.menu_main_cate ul.menu-right-head > li:last-child > a{border-right:none}nav.menu_main_cate ul.menu-right-head > li > a > span{text-align:center;position:relative;display:inline-block;vertical-align:middle;float:left}nav.menu_main_cate ul.menu-right-head > li > a > span img{display:inline-block;max-height:28px;max-width:30px}nav.menu_main_cate ul.menu-right-head > li > a > span{margin:0;font-size:15px;font-weight:normal}nav.menu_main_cate ul.menu-right-head > li > a i{font-size:24px;padding-right:0}nav.menu_main_cate ul.menu-right-head > li:hover > a{color:#fff}nav.menu_main_cate ul.menu-right-head > li:hover i.fa{color:#fff}.subcate{position:absolute;left:100%;top:0;margin-left:-1px;z-index:1;box-shadow:2px 2px 6px 0 rgba(17,29,43,.24);background-color:rgba(255,255,255,1);width:711px;display:none;border:1px solid #e5e5e5;-moz-transition:all .25s ease-in-out;-o-transition:all .25s ease-in-out;-webkit-transition:all .25s ease-in-out;-ms-transition:all .25s ease-in-out;transition:all .25s ease-in-out;min-height:488px}.bg-gray{background-color:#fbfbfb}.subcate .box_subcate{padding:12px 18px;float:left;min-height:488px;line-height:20px;width:33.333333%;box-sizing:border-box}.subcate .box_subcate .links-cate:nth-child(2){margin:10px 0 0 0;border-top:1px solid #e3e3e3;padding-top:15px}.subcate h3{font-size:14px;font-weight:bold;color:#4a4a4a;margin:0 0 8px 0}.subcate h3 a{color:#4a4a4a}.subcate ul{list-style:none;padding:0}.subcate ul li{padding:3px 0}.subcate ul li a{color:#303030;padding:0;display:block;font-size:14px}.subcate ul li:hover a{color:#4a90e2}.menu-main-left > li:hover .subcate{display:block}.banner-top{padding-top:0;overflow:hidden}.slide-banner{float:left;width:80%;padding:10px 10px 0 0;margin-left:0}.cate-right{float:right;width:20%}.cate-right img{width:100%}.is-affixed{height:auto !important}.er-top-nav{background:#fff;width:100%;-webkit-box-shadow:0 1px 3px 0 rgba(0,0,0,.08);box-shadow:0 1px 3px 0 rgba(0,0,0,.08)}.sub_header_hot img{width:100%}.sub_header{display:flex;vertical-align:middle;width:100%;background:#fff;border-bottom:3px solid #f34723;box-shadow:0 0 4px 0 rgba(152,165,185,.2);justify-content:space-between;align-items:center}.sub_header h2{font-size:22px;font-weight:600;position:relative;padding:0 10px;line-height:8px;float:initial;width:auto;margin:0;background:#f34723}.sub_header a{flex:1;margin:0;padding:0 3px;font-size:12px;text-align:center;display:flex;justify-content:center;align-items:center;min-height:45px;color:#333;border-right:1px solid #f3f3f3}.sub_header h2 a{color:white;border:none}.sub_header a:hover{transition:.3s;box-shadow:rgba(0,0,0,0.1) 0 0 20px}.sub_header a.active{background:url("https://www.dienmayxanh.com/content/images/2019/ThemeTet2020/active.png") no-repeat center center;background-size:cover;color:#fff;font-weight:600;font-weight:600}.sub_header a.viewall{border-radius:99px;padding:0 15px;background-color:#4a90e2;font-size:14px;color:#fff;text-align:center;line-height:32px}.sub_heading{display:flex;overflow:hidden;justify-content:space-between;align-items:center;padding:8px 0}.sub_heading h2{font-size:22px;color:#505050;font-weight:600;line-height:8px;width:auto;margin:0}.sub_heading .icon{display:flex;display:-webkit-flex;align-items:center}.sub_heading i{font-size:23px;margin:0 10px 0 0;width:40px;background:#6e58f1;line-height:40px;text-align:center;color:#fff;height:40px;border-radius:50%}.sub_heading i.akr-icon_CatNew{background:#f9a425}.sub_heading i.akr-icon_Clock{background:#eb86be}.sub_heading a.viewall{border-radius:99px;padding:0 15px;background-color:#4a90e2;font-size:14px;color:#fff;text-align:center;line-height:32px}#box_pro_flashsale li{float:none}#box_pro_flashsale_cm{clear:both}#box_pro_flashsale_cm li .bglipro{min-height:320px}.banner_adv_pro,.category_right{width:20%;float:right}.listproduct{display:block;overflow:hidden;margin:0 auto 10px;border-top:1px solid #eee}.list_border{border-left:1px solid #eee;border-right:1px solid #eee}.listproduct li{float:left;position:relative;overflow:hidden;cursor:pointer;border-right:1px solid #eee;border-bottom:1px solid #eee;padding:0;min-height:266px}.listproduct li .bglipro{display:block;overflow:hidden;background:#fff;cursor:pointer;padding-bottom:8px;min-height:266px}.listproduct li .bglipro p.idpro{padding:10px}.listproduct li .bglipro .reponsive-img{padding-top:65%;margin:25px 0 10px}.listproduct li .Parameter{position:absolute;top:0;right:10px;margin:0;text-align:right;display:block;overflow:hidden}.listproduct li .Parameter img{display:block;vertical-align:middle;width:auto;height:30px;margin:0}.listproduct li .Parameter h3{display:inline-block;font-size:14px;color:#4a90e2;font-weight:600;vertical-align:middle;display:none;border:1px dashed #4a90e2;border-radius:2px;line-height:18px;padding:0 3px;background:#fff}.c-product-item_info{padding:0 12px 10px}.listproduct li h3{display:block;overflow:hidden;line-height:1.3em;font-size:14px;color:#4a90e2;font-weight:500;height:36px}.price-box{padding:3px 0 6px;overflow:hidden;position:relative}.big-bn-right{float:right;width:20%}.homenews{margin-top:10px}.homenews h2{background:#fff;font-size:14px;padding:11px 0 8px 12px}.homenews h2 a:after{content:'';width:0;height:0;border-left:5px solid #4a90e2;border-top:5px solid transparent;border-bottom:5px solid transparent;display:inline-block;margin-left:5px}.homenews h2 a{color:#f34723;text-transform:uppercase;font-weight:600;line-height:20px}.homenews .more{float:right;padding:0 10px;background:#fff;font-size:12px;color:#4a90e2}.homenews li{display:block;padding:5px 10px 6px;border-top:1px solid #ededed;background:#fff}.homenews li a{display:block;overflow:hidden}.homenews li img{float:right;width:66px;height:auto;max-height:50px;margin-left:10px}.homenews li h3{display:block;overflow:hidden;height:35px;font-weight:normal;line-height:1.3em;font-size:14px;color:#666}.homenews li span{display:inline-block;font-size:11px;color:#999;vertical-align:middle}li.bannerHome img{height:auto;margin-top:3px}li.bannerHome{display:inline-block}.list-brand-cate{font-size:15px;margin:10px 0 0 0;padding-left:0;list-style:none}.list-brand-cate li{margin-bottom:10px;transition:0.3s;-webkit-box-shadow:0 2px 4px 1px rgba(0,0,0,.12);box-shadow:0 2px 4px 1px rgba(0,0,0,.12);border-radius:3px;background:#fff}.list-brand-cate li .category-card__image{position:relative;height:100%;width:100%;padding-top:58.33%;overflow:hidden}.list-brand-cate li .category-card__image img{object-fit:contain;margin:auto;padding:10px;position:absolute;width:100%;height:100%;top:0;left:0}.list-brand-cate li .category-card__name{border-top:1px solid #e6e6e6;padding:15px 8px;font-weight:600;text-align:center;line-height:1.25rem;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center}.list-brand-cate li a{font-size:14px;color:#212121;display:block;border-radius:3px;justify-content:center;align-items:center}.list-brand-cate li a:hover{color:#288ad6}.list-brand-cate li:hover{-webkit-box-shadow:0 2px 4px 2px rgba(0,0,0,.12);box-shadow:0 2px 4px 2px rgba(0,0,0,.12)}.list-brand-cate li:hover img{transition:0.5s;-webkit-transform:scale(1.1);transform:scale(1.1)}.search-ads{background:#fff;font-size:14px;position:relative;overflow:hidden;margin-top:20px;padding:10px}.search-ads > span{margin-right:15px}.search-ads > a{margin-right:25px;line-height:25px;font-size:14px;color:#4a90e2}.news-homepage{background:#fff;position:relative;overflow:hidden;margin-top:8px;padding:10px}.news-homepage img{vertical-align:middle;width:335px;height:421px;object-fit:cover;object-position:left}.news-homepage .fl > a img{width:100%;height:361px}.news-homepage .fl{margin-right:16px;height:auto;overflow:hidden;width:54%;float:left}.news-homepage .fl h4{color:#333;padding:5px 0;font-size:16px}.news-homepage .fl a h4{color:#333}.news-homepage .fr{height:auto;width:44.3%;display:inline-block}.news-homepage .fr .right-news > a{display:inline-flex;padding-bottom:3px;position:relative;overflow:hidden;width:100%}.news-homepage .fr .right-news > a:not(:last-child){border-bottom:1px solid #ededed}.news-homepage .fr .right-news > a{display:inline-flex;padding-bottom:3px;position:relative;overflow:hidden;width:100%}.news-homepage .fr .right-news figure{position:relative;width:150px}.news-homepage .fr .right-news > a:not(:first-child){padding-top:3px}.news-homepage .fr figure img{display:block;opacity:1;width:100%;height:66px}.news-homepage .fr .right-news > a > p{display:-webkit-box;width:calc(86% - 20px);cursor:pointer;font-size:14px;color:#333;padding:5px 10px;line-height:18px;-webkit-line-clamp:3;-webkit-box-orient:vertical;height:50px}.list-news-home{margin:0 -5px}.list-news-home .item{margin-bottom:10px;box-shadow:4px 4px 16px 0 rgba(217,228,231,.5);border-radius:6px;background:#fff}.list-news-home .item .img{position:relative;display:block;padding-top:65%;overflow:hidden}.list-news-home .item .img img{border-top-left-radius:8px;border-top-right-radius:8px;position:absolute;top:0;bottom:0;left:0;right:0;width:100%;min-height:100%}.list-news-home .item .time{width:50px;float:left;height:55px;border:solid 1px #ddd;text-align:center}.list-news-home .item .time b{display:block;font-size:21px;margin-top:3px;margin-bottom:2px}.list-news-home .col-md-3{padding:0 5px}.list-news-home .item .info{padding:10px}.list-news-home .item .info .cat{display:inline-block;background:#0c72bd;color:#fff;font-weight:500;line-height:24px;padding:0 10px;font-size:13px;margin-bottom:10px;border-radius:3px}.list-news-home .item .info a.name{font-size:16px;margin-bottom:10px;line-height:20px;color:#333;font-weight:bold;text-overflow:ellipsis;-webkit-line-clamp:2;-webkit-box-orient:vertical;display:-webkit-box;overflow:hidden}.list-news-home .item .info a.name:hover{color:#f00}.list-news-home .item .info p{font-size:14px;text-overflow:ellipsis;-webkit-line-clamp:3;-webkit-box-orient:vertical;display:-webkit-box;overflow:hidden}.social-footer{display:table;width:100%;margin-top:15px}.social-footer a{width:35px;height:35px;text-align:center;margin-right:10px;border-radius:50px;border:none;background:#e00;float:left;margin-right:10px}.social-footer a i.fa{height:35px;text-align:center;line-height:35px;color:#fff;font-size:18px;transition:all .5s}#footer{background:#fff;box-shadow:0 0 4px 0 rgba(152,165,185,.2);margin-top:20px}.footer_middle{background:#fff;border-top:1px solid #ccc}.footer-top-block-content{display:-webkit-box;display:-ms-flexbox;-webkit-box-align:center;-ms-flex-align:center;align-items:center;height:100%}.footer-top-block{height:90px;margin:0;border-bottom:1px solid #e8e8e8}.footer-content-block{background-color:#fff;margin-top:15px;margin-bottom:15px}.footer-top-block-item{text-transform:uppercase;font-size:13px;font-weight:700;color:#666;padding:0 17px;width:20%;border-left:1px solid #e8e8e8;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;height:100%}.ft-content-section{margin:auto}.footer-top-block-item i{font-size:35px;color:#f00;margin-right:15px}.footer-ad-words-block{width:100%;overflow:hidden;font-size:11px;padding:15px 0;border-bottom:1px solid #e8e8e8;background:#fff;text-align:justify}.footer-ad-words-block a{color:#000}.footer-ad-words-block a:hover{text-decoration:underline}.footer-ad-words-block span{padding:0 5px}.footer-top-block-right-content{width:100%}.ft-subscription-content{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;position:relative}.ft-subscription-content i.akr-icon_Envelope{position:absolute;font-size:22px;left:10px;top:8px;color:#aaa;margin-right:20px}.ft-subscription-input{width:100%;border:1px solid #e8e8e8;padding:0 15px 0 40px;border-right:0;font-size:13px;border-top-left-radius:3px;border-bottom-left-radius:3px;background-color:#fff}.ft-subscription-btn{width:100px;padding:9px 10px;background:#d00;text-align:center;color:#fff;cursor:pointer;text-transform:uppercase;font-size:13px;border-top-right-radius:3px;border-bottom-right-radius:3px;-webkit-transition:.1s ease-in-out;transition:.1s ease-in-out}.ft-subscription-text{font-size:13px;font-weight:700;margin-bottom:5px;display:block;color:#666}.ft-subscription-block i{font-size:60px;color:#aaa;margin-right:20px}.ft-subscription-block{width:60%;min-width:472px;padding:10px 30px 10px 15px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;height:100%;border-left:1px solid #e8e8e8;border-right:1px solid #e8e8e8}.footer_top{padding:10px 0 0;margin-top:15px}ul.list-footer > li{width:25%;float:left}ul.list-footer > li p.heading{color:#636363;margin-top:8px;position:relative;text-transform:uppercase;font-weight:normal;display:block;font-size:15px}ul.list-footer > li .bg-ft{color:#636363}ul.list-mn li{padding:5px 0;font-size:14px}ul.list-mn li a{color:#288ad6;font-size:14px}ul.list-mn li a strong{color:#f00}ul.list-mn li a:hover{color:#666}.payment-img{padding:10px 0}.payment-img img{display:inline-block;width:14%;margin-right:1%;margin-bottom:7px;vertical-align:middle}.social-foo{float:left;width:100%}.social-foo a{display:block;float:left;margin-right:10px}.social-foo a i.fa{width:46px;height:46px;background-color:#e23a24;-ms-border-radius:6px;border-radius:6px;text-align:center;line-height:46px;color:#fff;font-size:28px}.social-foo a i.fa:hover,.social-foo a i.fa:focus{background-color:#c42812}.footer_bottom{background:#f8f8f8;padding:10px 0 5px 0;color:#999;font-size:11px}.footer_bottom h5{font-size:11px;font-weight:600;margin:0}.footer_bottom p{font-size:11px;line-height:18px;font-weight:300;margin:0}.footer_bottom div.block-cpn{width:45%;float:left}.footer_bottom div.block{float:right}.footer_bottom img{height:40px}.footer_bottom p{margin:0}.adressshowroom{color:#fff;line-height:1.6em}.footer_address{border-top:1px solid #F1F1F1}ul.list_showroom > li{padding:13px 0 20px;float:left;width:33.33333333%}ul.list_showroom > li .bg_showroom{display:block;position:relative;text-decoration:none}ul.list_showroom > li .bg_showroom .img-thumb{float:left;width:40%;overflow:hidden}ul.list_showroom > li .bg_showroom .img-thumb img{width:100%}ul.list_showroom > li .bg_showroom .img-thumb .hm-reponsive{padding-top:116.25%}.info-showroom{float:left;width:60%;padding-left:7px}.info-showroom .name{color:#F25A29;font-weight:bold;background:url("/Content/desktop/css/../images/home.png") no-repeat 0 0;padding-left:22px;padding-top:1px;font-size:13px;text-transform:uppercase;background-size:19px;letter-spacing:-0.5px;margin-bottom:2px}ul.list-adress li{font-size:12px;color:#333;letter-spacing:-0.2px;padding-left:4px;line-height:16px;margin-bottom:5px}ul.list-adress li span,ul.list-adress li a{color:#F25A29}ul.list-adress li i.fa{padding-right:5px}.ft_diemban{background:#F25A29;border-radius:5px;padding:7px 8px;display:inline-block;color:#fff;font-size:11px}.menu_main_cate.fixed{position:fixed;width:100%;z-index:99;top:33px;background:#78B43D}.menu_main_cate.fixed .menu-main-left{display:none}.menu_main_cate.fixed .menu-right-head{display:none}.menu_main_cate.fixed nav.menu_main_cate{background:#eee !important}.header-wrap.fixed .headsearch{float:none;width:100%}.box-cate-main:hover .menu-main-left{visibility:visible !important}.box-cate-main{position:relative;width:20%;z-index:6;float:left}.box-cate-main .menu-main-left.home-active{display:block}.box-cate-main .nav-categories{font-size:15px;font-weight:600;height:44px;line-height:44px;text-transform:uppercase;padding-left:15px;cursor:pointer;border-bottom:none;color:#222;box-shadow:0 0 4px 0 rgba(0,0,0,.2);background:#fff}.box-cate-main .nav-categories i{font-size:18px;top:12px;right:10px;position:absolute}.box-cate-main .menu-main-left{position:absolute;left:0;top:100%;overflow:hidden;height:386px;width:260px;display:none;-moz-box-shadow:0 1px 2px 0 rgba(0,0,0,0.1);-webkit-box-shadow:0 1px 2px 0 rgba(0,0,0,0.1);box-shadow:0 1px 2px 0 rgba(0,0,0,0.1);background:#fff}.box-cate-main .menu-main-left:hover{overflow:visible;height:auto}.menu-main-left > li{cursor:pointer;white-space:nowrap;border-top:1px solid #dcdee3;text-overflow:ellipsis}.menu-main-left > li .menu-item.current{background:#fff;z-index:2}.menu-main-left > li:hover .menu-item::after{right:0;color:#f00}.menu-main-left > li .menu-item{display:flex;position:relative;padding:6px 10px;display:-webkit-flex;align-items:center}.menu-main-left > li .menu-item .icon{margin-right:10px;text-align:center}.menu-main-left > li .menu-item .icon img{height:22px;filter:opacity(.5)}.menu-main-left > li .menu-item::after{font-family:"FontAwesome";content:'\f105';color:#a6a6a6;font-size:18px;position:absolute;right:8px;transition:all .3s}.menu-main-left > li p{margin:0}.menu-main-left > li p a{font-size:14px;color:#3f3f3f}.menu-main-left li a:hover{color:#fe0000}.menu-main-left > li label{font-weight:400;margin:0;cursor:pointer}.box-cate-main:hover .menu-main-left{display:block}ul.service-item{display:table;width:100%;border:1px dotted #e5e5e5;background:#fff}ul.service-item li{float:left;height:65px;display:block;-moz-transition:all .3s ease;-o-transition:all .3s ease;-webkit-transition:all .3s ease;-ms-transition:all .3s ease;transition:all .3s ease;text-align:center;width:20%;border-right:1px solid #d4d4d4}ul.service-item li span{display:block;width:100%;text-align:center;font-size:30px;color:#404040;padding-top:5px;padding-bottom:5px}ul.service-item li small{font-size:14px;color:#404040;line-height:1.3em;padding:0 3px;display:block;text-align:center}ul.service-item li:hover{background-color:#f3f3f3}ul.service-item li:nth-child(5){border-right:none}.sologan-foo{padding-bottom:15px;background:#f2f2f2}.sologan-foo .item-sologan{float:left;width:100%;background-image:url("/Content/desktop/css/../images/i-footer-1.png");background-repeat:no-repeat;padding-left:64px;margin-top:15px;height:49px}.sologan-foo .item-sologan a{font-size:16px;padding-top:5px;text-transform:uppercase;color:#2c2c2c;font-weight:bold;float:left;display:block;width:100%}.sologan-foo .item-sologan a:hover{color:#e23a24}.sologan-foo .item-sologan span{font-size:14px;color:#818281}.sologan-foo .item-sologan.slg-1{background-position:0 0}.sologan-foo .item-sologan.slg-2{background-position:0 -49px}.sologan-foo .item-sologan.slg-3{background-position:0 -98px}.Wrapper{-moz-min-width:1300px;-ms-min-width:1300px;-o-min-width:1300px;-webkit-min-width:1300px;min-width:1300px}.Wrapper_cate{background:#fff}.listproduct li.col_w25{width:25%}.listproduct li.col_w25:nth-child(4n+4){border-right:0}.listproduct li.col_10_2:nth-child(5n+5){border-right:0}ul.listproduct-cate{border-left:1px solid #f3f3f3}.breadcrumb{display:block;overflow:hidden;background:none;margin:5px 0;line-height:32px;padding:0;float:left;width:100%}.breadcrumb > li{font-size:14px;float:left;color:#000}.breadcrumb > li + li:before{content:"\f105";padding:0 10px;float:left;font-family:"FontAwesome";font-size:20px;color:#999}.breadcrumb > li a{color:#999}.breadcrumb > li a:hover{color:#000}.leftcate{float:left;width:79%}.rightcate{float:right;width:20%}.listproduct li .bginfo{display:block;height:200px;background:#fff;font-size:12px;color:#fff;line-height:16px;position:absolute;top:0;left:0;right:0;bottom:0;z-index:6;-ms-opacity:0;opacity:0;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);pointer-events:none;-webkit-transition-duration:1s;-moz-transition-duration:1s;-o-transition-duration:1s;transition-duration:1s}.listproduct li .bginfo h4{padding:8px 5px;height:47px}.listproduct li .bginfo span{display:block;color:#666;padding:2px 5px;text-decoration:none;font-size:14px;line-height:20px}.listproduct li:hover .bginfo{-ms-opacity:1;opacity:1;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";filter:alpha(opacity=100);-khtml-opacity:1;-webkit-transition-duration:.5s;-moz-transition-duration:.5s;-o-transition-duration:.5s;transition-duration:.5s}.box-online{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px}.box-proside{display:block;font-size:14px;overflow:hidden;border:1px solid #ddd;border-radius:4px;position:relative;margin:0;background:#fff;margin:5px 0 5px;padding-bottom:10px}.box-proside aside{display:block;overflow:hidden;margin-top:10px}.box-proside aside p{font-size:12px;color:#666;padding-top:10px;border-top:1px dashed #c8c8c8;margin:0 10px}.box-proside .imgpromotion{display:block;overflow:hidden;width:calc(100% - 20px);white-space:nowrap;padding:10px 10px 0}.box-proside .imgpromotion img{display:block;width:60px;height:auto;margin:auto;float:left;margin-right:5px}.box-proside .imgpromotion h3{overflow:hidden;font-size:11px;color:#288ad6;white-space:initial;text-overflow:ellipsis;line-height:13px;margin-top:5px;display:-webkit-box;-webkit-line-clamp:3;-webkit-box-orient:vertical;max-height:40px}.box-proside .box-pd{padding:10px 10px 0 10px}.box-proside span.promo{display:block;overflow:hidden;padding:10px 10px 0 30px}.box-proside span i.numeric{width:16px;height:16px;display:inline-block;background:#468ee5;color:#fff;float:left;text-align:center;border-radius:50%;margin:3px 0 0 -24px;font-style:normal;border:1px solid #468ee5;border-top:0;font-size:11px}.box-proside a{color:#288ad6}.box-proside strong{display:block;overflow:hidden;font-size:16px;color:#333;line-height:15px;padding:8px 10px;text-transform:uppercase;font-weight:bold;margin-top:10px}.box-proside strong:first-child{margin-top:0;padding:10px 15px;text-transform:unset;background:#f34723;color:white;font-size:14px}.box-proside strong:first-child > span{font-size:14px;text-transform:none;font-weight:400;display:inline-block;padding-top:5px}.box-onlineheader{background-color:#e21d22;padding:10px;line-height:12px;position:relative;-moz-border-radius:4px 4px 0 0;-webkit-border-radius:4px 4px 0 0;border-radius:5px 5px 0 0}.box-onlineheader > img{width:94px;height:42px;display:inline-block;border-right:1px solid #fff;padding-right:10px;padding-top:0;margin-right:10px}.box-onlineheader > div{display:inline-block;vertical-align:top;margin-top:5px}.box-onlineheader label{display:block;text-align:left;color:#fff;font-weight:bold}.box-onlineheader label strong{font-size:22px;color:#f8e81c;text-align:left}.box-onlineheader span{font-size:13px;color:#fff;margin-top:5px}.box-onlinecontent{padding:10px;border:1px solid #fede9d;border-top:none;-moz-border-radius:0 0 5px 5px;-webkit-border-radius:0 0 5px 5px;border-radius:0 0 5px 5px;background:#fff4de}.box-online ul.area_promotion{margin:0;padding-bottom:10px}.box-online ul.area_promotion li{display:block;position:relative;font-size:14px;color:#333;padding:5px 0 5px 15px}.box-online ul.area_promotion li:before{content:"";width:5px;height:5px;border-radius:50%;background:#6d6d6d;left:0;position:absolute;top:12px}.box-online .shockbuttonbox{margin-bottom:10px}.shockbuttonbox{clear:both}.shockbuttonbox a.check-out{background-image:linear-gradient(-180deg,#e52025 2%,#d81116 96%);display:block;overflow:hidden;padding:9px 0;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;font-size:16px;line-height:normal;text-transform:uppercase;color:#fff;text-align:center}.shockbuttonbox a.buy_now1{display:block;overflow:hidden;padding:9px 0;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;font-size:16px;line-height:normal;text-transform:uppercase;color:#fff;text-align:center;background:-webkit-gradient(linear,0% 0,0% 100%,from(#fd6e1d),to(#f59000));background:-webkit-linear-gradient(top,#f59000,#fd6e1d);background:-moz-linear-gradient(top,#f59000,#fd6e1d);background:-ms-linear-gradient(top,#f59000,#fd6e1d);background:-o-linear-gradient(top,#f59000,#fd6e1d)}.shockbuttonbox a.check-out span,.shockbuttonbox a.check-out span{display:block;font-size:12px;color:#fff;text-transform:none}.shockbuttonbox .buy_ins.twoins{float:left;width:calc(50% - 5px);margin-bottom:10px;margin-right:10px;margin-top:10px}.buy_ins.twoins.nl{margin-right:0 !important}.shockbuttonbox .buy_ins{line-height:normal;display:block;padding:9px 0;text-align:center;margin:10px 0 0 0;background-image:linear-gradient(-180deg,#2a8cd8 0,#1276c5 99%);-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;color:#fff;font-size:16px;text-transform:uppercase}.shockbuttonbox .buy_ins span{display:block;font-size:12px;color:#fff;text-transform:none}.box-online a.danhsach{text-align:center;display:block;font-size:14px;color:#4a90e2;cursor:pointer;clear:both}.list_keys{max-height:60px;position:relative}.list_keys:before{content:"";position:absolute;bottom:0;left:0;width:100%;height:30px;background:linear-gradient(hsla(0,0%,100%,0),#fff)}.list_keys .title{font-size:16px;font-weight:600;padding:10px 0}.list_keys ul{display:block}.list_keys li{display:block;position:relative;font-size:14px;color:#333;padding:3px 0 3px 10px}.list_keys li:before{content:"";width:4px;height:4px;border-radius:50%;background:#999;left:0;position:absolute;top:10px}.box_content_view{background:#fff;padding:15px;margin-top:10px;box-shadow:0 0 4px 0 rgba(152,165,185,.2);border-radius:6px;display:block;float:left;width:100%}.wp-post-thongso{line-height:160%;font-size:16px}.wp-post-thongso p > img{width:auto;max-width:100%;margin:10px auto;display:block}.expand.js-content{overflow:hidden;margin-bottom:15px;max-height:960px}.expand.js-content p > img{width:auto;max-width:100%;margin:10px auto;display:block}.js-content.expand + .show-more::before{content:"";height:55px;margin-top:-45px;position:relative;background:-webkit-gradient(linear,0% 100%,0% 0,from(rgb(255,255,255)),color-stop(0.5,rgb(255,255,255)),to(rgba(255,255,255,0)));display:block}.show-more #js-show-more{display:block;color:#189eff;text-align:center;width:229px;border:1px solid #189eff;height:39px;line-height:39px;border-radius:4px;font-size:14px;font-weight:400;margin:15px auto 10px}.show-more{margin-bottom:30px;margin-top:0;color:#242424}.box-table-thongso .table-responsive table tr:nth-child(2n+1){background:rgba(0,0,0,.025)}.box-table-thongso .table-responsive table tr td{padding:6px 10px}.box-table-thongso .table-responsive table tr td:first-child{width:30%;font-weight:bold}.btn-xem-ct{margin-top:10px}.btn-xem-ct a{width:100%;transition:0.2s;border-color:#288ad6;color:#288ad6}.btn-xem-ct a:hover{background:#288ad6;color:#ffff}.sp-khuyenmai .btn-mua-ngay .btn{width:49%;float:left}.sp-khuyenmai .btn-mua-ngay .btn1{margin-right:2%}.left-km{display:table;width:100%}.left-km > a{display:block;width:100px;float:left}.left-km .text-11{width:calc(100% - 100px);float:left;padding-left:10px}.left-km .text-11 .h4-title a{color:#333;font-size:16px}.sp-khuyenmai{padding:10px 0;border-top:1px solid #ccc}.h3-title-spcl{color:#333;border-bottom:1px solid #ddd;margin-bottom:15px;padding-bottom:10px}.box-tinlq{margin:20px 0}.h3-title-sb-ct{color:#333;font-size:20px;margin-bottom:20px}.side-bar-ctsp > div{margin-bottom:20px}.list-tinlq li{display:table;width:100%;margin-bottom:15px}.list-tinlq li > a{display:block;width:100px;float:left}.list-tinlq li > .title-tin{width:calc(100% - 100px);float:left;padding-left:10px}.list-tinlq li > .title-tin .h3-title{font-size:15px;font-weight:400}.list-tinlq li > .title-tin .h3-title a{color:#333}.btn-mua-ngay{display:table;width:100%;margin:15px 0}.fs-comment{width:100%;display:table;border-radius:5px;border:1px solid #ddd;margin-bottom:15px}.fs-cm-c1{width:193px;display:table-cell;vertical-align:middle;text-align:center;font-size:12px;color:#4a4a4a;padding:10px;border-right:1px solid #d8d8d8}.fs-cm-c2{display:table-cell;vertical-align:middle;padding:15px;width:300px;border-right:1px solid #d8d8d8}.fs-cm-c3{display:table-cell;vertical-align:middle;padding:15px;text-align:center}.fs-cm-c3 h4{font-size:16px;color:#4a4a4a;margin-bottom:15px}.btn-send-r span{cursor:pointer;line-height:34px;padding:10px 35px;color:#fff;background-image:linear-gradient(-180deg,#e52025 2%,#d81116 96%);border-radius:3px}ul.rating2{padding:0}ul.rating2 li{float:left;margin:5px 3px 3px 0}ul.rating2 li i{font-size:1.2em;color:#ffa700}ul.rating2 li i.none{color:#ccc}.rating > input{display:none}.rating > label{color:#ddd;float:right;margin-bottom:0;line-height:1}.rating > label:before{margin:5px 10px 5px 0;font-size:1.6em;font-family:'FontAwesome';display:inline-block;font-weight:900;content:"\f005"}.rating > .half:before{font-family:'FontAwesome';content:"\f089";position:absolute}.rating > input:checked ~ label,.rating:not(:checked) > label:hover,.rating:not(:checked) > label:hover ~ label{color:#FFD700}.rating-at > li label{color:#282828;width:54px;text-align:center;font-weight:600;margin:0}.rating-at > li div{margin:0 10px;display:inline-block;min-width:5px;height:10px;background:-webkit-linear-gradient(top,#eee,#f6f6f6);background:linear-gradient(to bottom,#eee,#f6f6f6);background-color:#f3f3f3;border-radius:4px;box-shadow:inset 0 1px 2px rgba(0,0,0,.1),inset 0 0 0 1px rgba(0,0,0,.1);width:100%;flex:auto;overflow:hidden}.rating-at > li div span{-webkit-transition:width .5s ease;transition:width .5s ease;float:left;font-size:0;height:100%;border-radius:4px;background:#fb0;background:-webkit-linear-gradient(top,#ffce00,#ffa700);background:linear-gradient(to bottom,#ffce00,#ffa700);background-color:#ffce00;display:block}.rating-at > li .count{color:#323431;width:123px;font-size:12px}.rating-at > li{display:flex;align-items:center;justify-content:space-between;margin-bottom:5px}.wp-rating-left{padding:10px;border:1px solid #ccc}.wp-rating-right{margin-top:10px}.start-1 fieldset.rating{float:left;margin-top:-5px}.er-titte-cm h2{display:block;margin-bottom:15px;border-top:1px solid #ccc;padding-top:10px;line-height:1.3em;font-size:20px;color:#333}.rating-send{display:none;padding:0}.start-1 > span{display:block;margin-bottom:10px;font-size:15px}.start-1 > p{font-size:15px;font-weight:600;float:left;margin:0 12px 0 0}.start-1 > p.view-star{font-size:40px;color:#ffa700;float:none;margin:5px 0 0 0;line-height:40px}.comment-at{display:table;width:100%;margin-top:15px}.comment-at textarea{height:100px !important;margin-top:10px}.btn-cmt{padding:10px 20px;border:none;color:#fff;margin-top:10px;background-image:linear-gradient(-180deg,#e52025 2%,#d81116 96%);border-radius:3px}.media-at{display:table;width:100%;padding:10px 0;border-bottom:1px dotted #ccc}.comava-at{background:#4a90e2;float:left;font-size:12px;font-weight:bold;height:40px;line-height:40px;margin-right:10px;text-align:center;width:40px;color:#fff;border-radius:100%}.combody-at{display:block;width:calc(100% - 50px);margin-left:0;float:left}.listrep-at{margin:15px 0 0 50px;background:#f8f8f8;padding:10px 15px 0 12px;overflow:hidden;border-radius:5px;clear:both;border:1px solid #dfdfdf}.comact-at .time-at{font-size:12px;color:#999}.combody-at p{margin-bottom:0;clear:both}.qtv-at{background:#E91C24}.combody-at strong i{padding:1px 5px;font-size:11px;font-weight:normal !important;display:inline-block;margin-left:10px;color:#fff;border-radius:4px;background:#ffa63e}.comact-at .reply-at{color:#E91C24;cursor:pointer;font-size:12px;float:right}.comment-at.repbox-at{display:none}.comact-at{padding-top:5px}.box-cmt-danhgia{margin-top:15px}.btn-mua-ngay .btn1{width:100%;float:left;background:-webkit-linear-gradient(top,#f59000,#fd6e1d);transition:all 0.3s ease-in-out;margin-bottom:10px}.btn-mua-ngay .btn2{width:100%;background-image:linear-gradient(-180deg,#2a8cd8 0,#1276c5 99%);transition:all 0.3s ease-in-out;color:#fff}.btn-mua-ngay .btn{font-size:14px;text-transform:uppercase;color:#fff;font-weight:bold}.btn-mua-ngay .btn span{display:block;font-size:12px;font-weight:normal;text-transform:none}.wp-btn-ktbh .btn{background:#2092fc;border-radius:6px;transition:all 0.3s ease-in-out;width:90%;margin:1%}.wp-btn-ktbh .btn:hover{background:#b62807}.review-article{line-height:24px;padding-right:20px;text-align:justify}.wp-item-tin-page{display:table;width:100%;padding-bottom:30px;margin-bottom:30px;border-bottom:1px solid #f1f1f1}.wp-item-tin-page .img-item-tin-page{width:380px;height:235px;float:left;margin-right:30px;overflow:hidden}.wp-item-tin-page .img-item-tin-page img{width:100%;height:100%;object-fit:cover;transition:all 0.6s ease-in-out}.wp-item-tin-page .img-item-tin-page:hover img{transform:scale(1.06);opacity:0.8}.wp-item-tin-page .text-item-tin-page{width:calc(100% - 410px);float:left}.wp-item-tin-page .text-item-tin-page .h3-title{font-size:18px;margin-bottom:10px}.wp-item-tin-page .text-item-tin-page .h3-title a{color:#000}.wp-item-tin-page .h3-title:hover a{color:#EA1C24}.wp-item-tin-page .text-item-tin-page .p-date{font-size:12px;color:#6D6E71;padding-bottom:10px;margin-bottom:15px;border-bottom:1px solid #f7f7f7}.wp-item-tin-page .p-date i{font-size:14px;color:#EA1C24;margin-right:5px}.wp-item-tin-page .text-item-tin-page .p-post{color:#414042;font-size:13px;display:-webkit-box;overflow:hidden;-webkit-box-orient:vertical;text-overflow:ellipsis;-webkit-line-clamp:4;word-break:break-word;margin-bottom:20px}.wp-item-tin-page .btn-xem-them a.xem-them{font-size:12px;border-radius:0;color:#58595B;border:1px solid #eaeaea;padding:6px 20px;transition:all 0.3s ease-in-out}.wp-item-tin-page .btn-xem-them a.xem-them:hover{background:#EA1C24;color:#fff;margin-left:10px}.wp-item-tin-page a.xem-them i{color:#EA1C24;margin-left:5px;font-size:8px}.wp-item-tin-page a.xem-them:hover i{color:#fff}.pagination-center{text-align:center}.pagination{margin:5px 0 20px 0}.pagination > li > a,.pagination > li > span{margin:0 5px;border-radius:0 !important;color:#4e4e4e}.pagination > .active > a,.pagination > .active > a:focus,.pagination > .active > a:hover,.pagination > .active > span,.pagination > .active > span:focus,.pagination > .active > span:hover{background:#EA1C24;border-color:#EA1C24}.other-news-detail{position:relative;padding:0}.other-news-detail h2{font-size:18px;border-bottom:1px solid #ccc;font-weight:600;color:#e00;margin-bottom:20px;text-transform:uppercase;position:relative;padding-bottom:20px}.other-news-detail h2 span{background:#fff;padding-right:10px;position:absolute;top:12px;left:0}.other-news-detail ul li{padding:5px 0 5px 0;display:block;position:relative}.other-news-detail ul li a{color:#333;text-decoration:none;font-size:14px;display:block;padding-left:18px}.other-news-detail ul li a:hover{text-decoration:underline}.other-news-detail ul li:before{content:"";position:absolute;top:11px;left:2px;width:4px;height:4px;background:#ccc}.new-list{display:block;overflow:hidden;margin:20px 0 0;border-top:1px solid #e4e4e4;padding-top:20px}.new-list li{float:left;width:48%;overflow:hidden;padding:10px 0}.new-list span.txt{display:block;overflow:hidden;font-size:17px;color:#333;line-height:1.3em}.new-list li a{display:block;overflow:hidden;color:#4a90e2;padding:6px 0;line-height:18px}.new-list li a:before{content:'?';font-size:12px;margin-right:5px;color:#d8d8d8}.productrelate strong{display:inline-block;vertical-align:middle;font-size:16px;color:#333}#brand-home{background:#fff;font-family:'Roboto',sans-serif;overflow:hidden;margin-top:20px;box-shadow:0 2px 7px 0 #a6a6a6}#brand-home h2{text-transform:uppercase;font-weight:500;font-size:20px;margin:15px 0;border-bottom:solid 1px #ddd;padding-bottom:8px}#brand-home ul{display:flex;padding:0;margin:0 -15px;flex-direction:row;list-style:none}#brand-home ul li{width:20%;margin:15px;background:#fff;border:1px solid #ddd;text-align:center}ul.listproduct-detail{border:none}ul.listproduct-detail li{border:1px solid #fff}ul.listproduct-detail li:hover{box-shadow:0 2px 4px 0 rgba(220,220,220,.5);border:1px solid #e8e8e8}ul.listproduct-detail .c-product-item_info{padding:0 10px 10px}ul.listproduct-detail .bginfo_dt{overflow:hidden;padding-top:3px}ul.listproduct-detail .bginfo_dt span{display:block;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;font-size:12px;color:#666;padding:3px 0;line-height:normal;text-transform:lowercase}.product-detail-block{background:#fff;padding:15px;box-shadow:0 0 4px 0 rgba(152,165,185,.2);border-radius:6px}.productdecor-details{clear:both}.productdecor-details h1{font-size:22px;font-weight:normal;color:#333;float:left}.productdecor-price{clear:both;padding:9px 0;line-height:20px}.productdecor-price i{vertical-align:middle;font-size:18px;margin-right:4px}.productdecor-price a{color:#dd1015;font-size:11px}.productdecor-price .price del{margin:0;display:inline-block;padding:0;color:#999;font-size:15px;font-weight:400}.productdecor-price .price span b{margin:0;display:inline-block;padding:0;color:#e00;font-size:15px;font-weight:400}.productdecor-price .price{font-size:23px;color:#e00;display:unset;font-weight:bold}.productdecor-details .product-sets{border:0;clear:both;border-width:0 0 1px}#product-actions-fieldset{border:0;margin:0;padding:0}#product-actions-fieldset button{font-size:16px;font-weight:bold;background-image:linear-gradient(-180deg,#e52025 2%,#d81116 96%);color:#fff;text-transform:uppercase;position:relative;padding:6px 38px;width:100%;border-radius:5px;box-shadow:0 3px 4px 0 rgba(10,31,68,0.1),0 0 1px 0 rgba(10,31,68,0.08);float:left;margin-top:10px;border:0}#product-actions-fieldset button i{position:absolute;top:13px;left:10px;font-size:20px}#product-actions-fieldset button span{display:block;font-size:10px;font-weight:400;text-transform:none}#product-actions-fieldset button.btn-cart{background-image:linear-gradient(-180deg,#2a8cd8 0,#1276c5 99%)}.why-buy label{font-size:12px;font-weight:500;background:#f34723;color:#fff;margin-bottom:10px;text-transform:uppercase;position:relative;padding:8px 15px;width:100%;border-radius:3px;box-shadow:0 3px 4px 0 rgba(10,31,68,0.1),0 0 1px 0 rgba(10,31,68,0.08)}.map-bt label{font-size:12px;font-weight:500;background:#f34723;color:#fff;margin-bottom:10px;text-transform:uppercase;position:relative;padding:8px 15px;width:100%;margin-top:10px;border-radius:3px;box-shadow:0 3px 4px 0 rgba(10,31,68,0.1),0 0 1px 0 rgba(10,31,68,0.08)}.wsupport-s{margin:0 -1%;text-align:center;overflow:hidden;clear:both}.wsupport-s li{width:31%;border:1px solid #ddd;border-radius:5px;list-style:none;padding:12px 0;-webkit-box-shadow:0 1px 3px 0 rgba(0,0,0,.08);box-shadow:0 1px 3px 0 rgba(0,0,0,.08);margin:1%;float:left}.wsupport-s li p{font-size:11px;line-height:1.2em;margin:0}.wsupport-s li i{display:block;font-size:26px;margin-bottom:5px}.rating-sets{padding-top:10px;clear:both}ul.rating-result{padding:0}ul.rating-result li{float:left;margin:0 6px 0 0}ul.rating-result li i{font-size:1.2em;color:#FFD700}ul.rating-result li i.none{color:#ccc}.whotline{margin:10px -5px;overflow:hidden;text-align:center;clear:both}.whotline li{width:47.2%;border:1px solid #ddd;border-radius:5px;list-style:none;padding:12px 0;-webkit-box-shadow:0 1px 3px 0 rgba(0,0,0,.08);box-shadow:0 1px 3px 0 rgba(0,0,0,.08);background:#f7f7f7;margin:0 5px;float:left}.whotline li span{display:block;font-size:12px}.whotline li p{margin:0}.whotline li p.tdtv{font-size:22px;font-weight:600;color:#e00}.whotline li p.hotline{font-size:22px;font-weight:600;color:#2a8cd8}.total-price,.total-payment{border-top:1px solid #e1e1e1;float:left;padding:10px 0;width:100%}.total-price span,.total-payment span{float:left;width:40%;font-size:14px}.total-price strong,.total-payment strong{float:right;font-weight:bold;text-align:right;width:60%}.total-payment span{font-weight:bold}.total-payment strong{color:#e23a24}.bt-payment{background-color:#e23a24;border:0 none;border-radius:3px;color:#fff;font-size:16px;font-weight:bold;margin-top:10px;padding:10px 12px;text-align:center;text-transform:uppercase}.bt-payment:hover,.bt-payment:focus{background-color:#333d94}.boxNewscate{margin-bottom:20px}.left-new{width:20%;padding-right:10px}.cent-new{width:56%;padding:0 20px 20px 20px;border-left:1px solid #efeded;border-right:1px solid #efeded}.right-new{width:24%;padding-left:25px}ul.list-new-cate li{padding:30px 0;overflow:hidden;border-bottom:1px solid #eee}ul.list-new-cate li figure{position:relative;overflow:hidden;float:left;width:100%}ul.list-new-cate li figure a{display:block}ul.list-new-cate li figure img{max-width:100%;max-height:190px;margin:auto;display:block}ul.list-new-cate li .blogtitle{margin-bottom:7px;display:block;line-height:1.2em;font-size:15px;color:#636363;font-weight:600;letter-spacing:.5px}ul.list-new-cate li .blogsummary{color:#4d4d4d;font-size:14px;padding-bottom:8px;line-height:22px;max-height:40px;overflow:hidden}ul.list-new-cate li .linkxemcht{color:#167ac6;display:inline;float:right}.new-list-cate h3{font-size:21px;line-height:30px;margin-bottom:6px;color:#333;font-weight:400}ul.items-new li{padding-top:10px;color:#999}ul.items-new li a{color:#288ad6;line-height:20px;font-size:14px}ul.items-new li a:before{content:'?';font-size:12px;margin-right:5px;color:#d8d8d8}.thumbnail-news-view{position:relative;font-size:15px;background:#fff}.thumbnail-news-view > h1{font-size:22px;font-weight:bold;color:#000;padding-bottom:12px}.p-date-ct{margin-bottom:20px;font-size:12px;color:#6D6E71}.p-date-ct i{font-size:14px;color:#EA1C24;margin-right:5px}.div-post-border{font-size:15px;font-weight:500;padding:20px;margin-bottom:25px;border:1px dashed #EA1C24}.post_content{word-wrap:break-word;padding-bottom:20px;line-height:25px}.post_content p > img{width:auto;max-width:100%;margin:10px auto;display:block}.post_content p{margin-bottom:10px}ul.items-new-02 li{padding-top:20px;clear:both;width:100%;height:80px}ul.items-new-02 li a{color:#288ad6;line-height:20px;font-size:14px}ul.items-new-02 li img{float:left;margin-right:10px;width:115px;max-height:75px;border-radius:4px}ul.items-new-02 li span{display:inline-block;line-height:20px;color:#999;margin-left:10px}ul.items-new-02 li span i.fa{padding-right:4px}.content-km{width:70%;float:left}.right-km{width:30%;padding-left:25px}.title_02 .text{color:#000;font-size:17px;font-weight:bold;margin-bottom:10px;text-transform:uppercase;line-height:35px;border-bottom:1px solid #f3f3f3;font-family:'Roboto Condensed',sans-serif}.list_manufacturer{border-top:1px solid #f3f3f3;border-left:1px solid #f3f3f3}.list_manufacturer li{border-bottom:1px solid #f3f3f3;border-right:1px solid #f3f3f3;float:left;text-align:center;width:25%;padding-bottom:15px}.brand-logo{position:relative;width:100%;overflow:hidden;height:174px}.brand-logo img{margin:auto;position:absolute;max-width:100%;max-height:80%;top:0;left:0;right:0;bottom:0;transition:all 0.5s ease 0s;-moz-transition:all 0.5s ease 0s;-webkit-transition:all 0.5s ease 0s;-ms-transition:all 0.5s ease 0s;-o-transition:all 0.5s ease 0s}.brand-logo img:hover{opacity:1;transform:scale(1.05);-webkit-transform:scale(1.05);-moz-transform:scale(1.05);-ms-transform:scale(1.05);-o-transform:scale(1.05)}.logo_home{text-indent:-9999px;font-size:0}span.priceline{display:inline-block;text-decoration:line-through;vertical-align:middle;font-size:15px;font-weight:500}a.bt-payment.buylink.other-buy{background-color:#e0ad00}a.bt-payment.buylink.other-buy:hover{background-color:#333d94}.notice-bc{clear:both}.notice-bc a{margin-right:10px}.payment-etho{clear:both;display:flex;padding:10px 0;text-align:center}.payment-etho .item{float:left;padding:0 20px 0 0;text-align:center;margin:5px 0}.payment-etho .item i{font-size:25px}.payment-etho .item p{font-size:12px;margin:0;line-height:13px}.showroom-add{padding:0 20px 20px 20px;border-radius:6px;margin-bottom:20px}.footer-new-left,.footer-new-center,.footer-new-right{position:relative;color:#f34723}.footer-new-left h3{text-align:left;font-size:14px;font-weight:bold;background:#f44723;color:white;padding:5px}.footer-new-right h3{font-size:15px;text-transform:uppercase;font-weight:bold}.footer-new-left button .icon-1{font-size:20px;padding-top:2px;float:right}.footer-new-left button .icon-2{font-size:20px;padding-top:3px;padding-right:10px}.footer-new-left button{background:none;border:none;border-top:1px solid #ccc;text-align:left;width:100%;font-size:10px;padding:0 0 5px 0;font-weight:bold}.footer-new-center h3{text-align:left;font-size:15px;text-transform:uppercase;margin-top:20px;margin-bottom:15px;font-weight:bold}.footer-new-center p{font-size:14px;margin:0 0 10px 0;line-height:20px}.footer-new-center p a,.footer-new-center a:hover{color:#333}.footer-new-center hr{margin:0}.footer-new-center img,.footer-new-right iframe{position:absolute;margin:auto;bottom:20px;left:0;right:0;padding:0 15px}.footer-new-center img{width:100%}.ftc-map{float:right;margin:10px 0 0;font-size:15px}.ftc-gt{float:left;margin:10px 0 0;font-size:15px}ul.address-dk > li{display:inline-table;border-radius:5px;min-height:130px}.address-dk .ft_img{float:left;padding-right:10px;width:36%;height:220px;position:relative}.address-dk .ft_img img{max-width:100%;max-height:100%;border-radius:5px 0 0 5px;position:absolute;top:0;left:0;right:0;bottom:0;margin:auto}.diachishowroom *,ul.list-footer *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.red{color:#ee3124}.title-store{font-size:20px;margin:0 0 15px 0;text-transform:uppercase;font-weight:600;word-spacing:2px;text-align:center;border-bottom:1px solid #ccc;padding-bottom:10px}.store-info{vertical-align:top;font-size:12px;color:#333}h4.name-showroom{text-transform:uppercase;font-size:15px;font-weight:bold;margin-top:25px;color:#222;line-height:18px}p.name-add{text-transform:uppercase;font-size:14px;font-weight:bold;margin:0;color:#4a90e2;margin-top:10px;line-height:18px}.store-info p{margin-bottom:0;line-height:21px}.store-info .signal{font-size:12px;padding:3px 5px;color:#fff;background:#f5791f !important}.gmap_box{display:none}.logo_home{display:none}#box_pro_special,#box_giasoc{clear:both}#box_pro_special .owl-carousel .owl-stage-outer{border:5px solid #f34723}.category_left{margin-right:1%;float:left;width:79%}#box_pro_special ul.list_product_featured li,#box_giasoc ul.list_product_featured li{width:100%;float:none;height:360px;border:none;padding:10px;border-right:solid 1px #eee;border-bottom:none}#box_pro_special ul.list_product_featured li:hover,#box_giasoc ul.list_product_featured li:hover{box-shadow:rgba(0,0,0,0) 0 0 0}#box_pro_special .owl-item img{width:auto;display:inline-block}.main-category{clear:both;border-radius:6px;box-shadow:1px 1px 3px 0 rgba(217,228,231,.5);background:#fff;overflow:hidden;padding:15px;margin-top:10px}.eurocook-page{display:block;float:left;width:100%;padding:0}.eurocook-page-view{clear:both;padding:0}.er-sidebar{width:280px;display:inline-block}.er-item{padding:15px;display:inline-table;width:100%;border-bottom:2px solid #f3f3f3}.er-item h3{color:#3f3f3f;font-size:18px;font-weight:bold;margin:0 0 10px 0}.er-item p{color:#a6a6a6;font-size:14px}.er-item .choosedfilter a{position:relative;border-radius:4px;background-color:#569efc;color:#fff;line-height:1.2;padding:5px 40px 5px 8px;margin:0 8px 8px 0;display:inline-block;vertical-align:middle;font-size:14px}.er-item .choosedfilter a .remove_filter{font-size:16px;position:absolute;top:13px;transform:translateY(-50%);right:0;color:#fff;background:#0a5bff;width:26px;height:26px;text-align:center;font-weight:bold;line-height:28px;border-top-right-radius:4px;border-bottom-right-radius:4px}ul.listform_filter{width:100%;float:left}.listform_filter li{width:50%;float:left}.listform_filter li .checkbox label::before,.listform_filter li .checkbox label::after{top:5px}.listform_filter li{margin:3px 0}.listform_filter li input[type="checkbox"]{display:none}.listform_filter li label{cursor:pointer;font-weight:normal}.listform_filter li input[type="checkbox"] + span::before{cursor:pointer;font-family:fontAwesome;font-size:12px;color:#000;content:"\a0";border:1px solid #999;box-shadow:0 1px 2px rgba(0,0,0,0.05);border-radius:2px;display:inline-block;text-align:center;height:18px;line-height:16px;width:18px;margin-right:8px;position:relative}.listform_filter li input[type="checkbox"]:checked + span::before{color:#fff;border:1px solid #41b948;background:#41b948;display:inline-block;content:'\f00c';font-size:12px}.listform_filter li a.filter-item span{display:inline-flex;color:#000;font-size:14px}.listform_filter li a.filter-item span,.listform_filter li a.filter-item img{vertical-align:middle}img.imgBrand{height:36px;object-fit:contain}.er-box-cate{border-radius:4px;box-shadow:0 0 4px 0 rgba(0,0,0,.08);background-color:#fff;width:100%;display:flex;display:-webkit-flex;align-items:center;justify-content:space-between;margin:0 !Important}.er-box-cate ul.list_product_featured li{width:20% !important;height:390px !important}.header-sort-cate{border-radius:6px 6px 0 0;background-color:#fff;box-shadow:0 0 4px 0 rgba(0,0,0,.08);border-bottom:1px solid #eee;width:100%;text-align:center;align-items:center;justify-content:space-between;padding:15px}.er-sort-cate{display:flex;display:-webkit-flex;align-items:center}.er-sort-cate h3{color:#000;font-size:14px;font-weight:bold;float:left}.er-sort-cate ul{display:flex;display:-webkit-flex;align-items:center}.er-sort-cate ul li{padding:0 0 0 20px}.er-sort-cate ul li a{color:#000;font-size:14px;display:flex;display:-webkit-flex;align-items:center}.er-sort-cate ul li:hover a::before,.er-sort-cate ul li.active a::before{content:"\f192";font-family:"FontAwesome";color:#ff910d}.er-sort-cate ul li a::before{content:"\f10c";font-family:"FontAwesome";margin:0 8px 0 0;font-size:16px;color:#a6a6a6}.page-pro-user{background-color:#fff;width:100%;text-align:center;align-items:center;padding:15px 15px 0 15px}.page-sort-cate{border-radius:0 0 6px 6px;box-shadow:0 0 4px 0 rgba(0,0,0,.08);background-color:#fff;width:100%;text-align:center;align-items:center;justify-content:space-between;padding:15px 15px 10px 15px;display:block;float:left}.page-sort-cate .pagination{margin:0}.box-category{clear:both;margin-top:10px;display:block;float:left;width:100%}.box-category.box-shadow{background:#fff;overflow:hidden;padding:10px;border-radius:6px;box-shadow:0 0 4px 0 rgba(152,165,185,.2)}.box-category div.list_product_hot{border:2px solid #f34723}.box-category ul.list_product_featured{display:block;float:left;width:100%;background:white}.box-category ul.list_product_featured li{float:left;position:relative;width:16.666666777%;border-right:1px solid #eee;border-bottom:1px solid #eee;height:370px;padding:10px;cursor:pointer;background:#fff}.box-category ul.list_product_featured li .iconup{width:100%;position:relative;height:20px;text-align:left}.box-category ul.list_product_featured li .iconup img{padding:0 !important;position:initial !important;margin:0 !important;height:100% !important;width:auto !important}.box-category ul.list_product_featured li:hover{transform:translateY(0px);transition:.3s;box-shadow:rgba(0,0,0,0.1) 0 0 20px;z-index:1}.product-cate li{border:none !Important;border-radius:8px;box-shadow:0 0 4px 0 rgba(0,0,0,.08);width:100% !important;float:none !important}.box-category .owl-item img{width:auto;display:inline-block !important}.cate_pro_top{float:left;width:100%}.cate_pro_top figure{position:relative;height:auto !important;width:100%;padding-top:100%;overflow:hidden}.cate_pro_top figure img{margin:auto;position:absolute;max-width:100%;max-height:100%;top:0;left:0;right:0;bottom:0}ul.product-cate li:hover{transform:translateY(-4px);transition:.3s;z-index:1}.cate_pro_top h3{line-height:12px;text-overflow:ellipsis;-webkit-line-clamp:2;font-size:14px;padding-top:5px;-webkit-box-orient:vertical;display:-webkit-box;overflow:hidden;height:36px}.cate_pro_top h3 a{font:14px/16px arial;color:#444}.cate_pro_title{margin:0;clear:both}.cate_pro_title img{display:inline-block;height:30px;vertical-align:middle}.cate_pro_gif{float:left;width:100%;height:88px;padding:10px}.cate_pro_gif strong{position:absolute;font:12px/20px arial;padding:0 8px;background:#fff;left:30px;top:-10px;color:#78B43D;font-weight:bold}.cate_pro_gif strong:before{content:"\f06b";font:15px/20px FontAwesome;color:#78B43D;margin-right:5px}.cate_pro_bot{width:100%;background:#fff;clear:both}.rating-lst{padding:8px 0}.rating-lst span{text-decoration:none !important;font-size:12px !important}.rating-lst > span:not(.sl-rating){color:#fc9639 !important}.rating-lst > span > b{color:#fc9639 !important}.rating-lst > span.sl-rating{color:#666 !important;margin-left:5px !important;vertical-align:top !important}.cate_pro_gif .bpq{border-top:1px solid #ddd;display:table;padding:15px 10px;width:100%;font:12px/20px arial;position:relative;text-align:left}.cate_pro_gif .bpq a{color:#000}.cate_pro_gif .bpq img{max-height:50px;margin-right:15px;float:left}.cate_pro_gif .bpq b:last-child{color:#f00}.cate_pro_bot p.price-now{color:#e00;font:bold 16px/22px arial;margin:0}.cate_pro_bot .promotion{overflow:hidden;font-size:12px;color:#333;margin:5px 0;font-weight:300}.cate_pro_bot .promotion b{color:forestgreen}.cate_pro_bot span{color:#999;font:14px/18px arial;text-indent:3px;text-decoration:line-through}.gift-promotion{color:#1fb349;font-size:13px;overflow:hidden;text-indent:12px;text-overflow:ellipsis;width:100%;height:18px}.gift-promotion:before{content:"\f06b";font:15px/20px FontAwesome;color:#1fb349;margin-right:5px}.cate_pro_bot-saleof{text-decoration:none !important;float:none !important;color:#e00 !important;font-weight:bold !important}.caption{text-align:center;padding-right:0;padding-left:0}.auth-block__menu-list{list-style:none;display:flex;height:60px;border-bottom:1px solid #eee}.auth-block__menu-list li{flex:1 1;text-align:center;border-right:1px solid #eee;position:relative}.btn-link-style{margin-top:0;color:#4a90e2;font-size:13px;font-weight:normal}.auth-block__menu-list a{display:flex;height:100%;width:100%;align-items:center;justify-content:center;font-size:16px;line-height:22px;color:#999}.auth-block__menu-list li.active a{font-weight:500;color:#303846}.auth-block__menu-list li:last-child{border-right:0}.auth-block__menu-list li.active:before{content:"";position:absolute;height:1px;left:30px;right:30px;bottom:-1px;background-color:#e00}.login-with__title{text-align:center;position:relative;color:#999;margin:15px 0}.login-with__title-inner{display:inline-block;position:relative;z-index:2;padding:0 10px;background:#fff}.login-with__title:before{content:"";width:100%;top:50%;margin-top:-.5px;height:1px;background:#eee;position:absolute;left:0}.auth-btn{display:flex;justify-content:center;align-items:center;width:100%;height:34px;color:#303846;font-size:13px;border:1px solid #e1e1e1;border-radius:30px}.auth-btn.-fb:hover{color:#fff;background:#315caf;border-color:#315caf}.auth-btn.-gp:hover{color:#fff;background:#ea3731;border-color:#ea3731}.auth-btn i{margin-right:5px;font-size:16px}.auth-btn.-fb{color:#315caf}.auth-btn.-gp{color:#ea3731}.event-info-head{width:100%;font-size:16px;text-transform:uppercase;text-align:center;margin:auto;padding-top:10px;padding-bottom:5px;margin-bottom:20px;background:#df242b;color:#fff;z-index:2;position:relative;padding-top:4px;padding-bottom:5px}.event-info-content{line-height:1.8em;position:relative;padding:15px;margin-top:-20px;z-index:1;padding-top:40px;padding-bottom:20px;font-size:14px}.form-list .form-control,.comment-at .form-control{box-shadow:none;padding:5px 10px;border-radius:3px;border:1px solid #e1e1e1;outline:none;height:40px}.form-list .form-control:hover,.comment-at .form-control:hover{box-shadow:0 0 0 3px hsla(0,0%,88.2%,.3)}.er-left{width:240px;vertical-align:top;display:inline-block}.er-right{float:right;width:calc(100% - 240px);width:-webkit-calc(100% - 240px);padding:0 0 0 15px}.orderbox{display:table;max-width:710px;margin:0 auto;padding-bottom:40px}.navigate{margin:0;float:left;width:100%}.navigate a{text-align:right;text-indent:20px;font-weight:400;line-height:40px}.navigate a:before{font:normal 14px/1 FontAwesome;margin:0 5px;content:"\f104"}.navigate label{float:right;display:inline-block;font:13px/40px 'Roboto';color:#666;margin:0}.boxFormPayment{float:left;background:#fff;border:1px solid #d8d8d8;box-shadow:0 0 20px rgba(0,0,0,.15);padding:10px 20px 20px 20px;display:table;width:100%}.listcart .quantity{height:40px;width:96px;float:right;position:absolute;right:0;top:45px;border-radius:3px;border:solid 1px #c8c9c6;background-color:#fff;box-shadow:none}.listcart .inc.qtybutton,.boxFormPayment .dec.qtybutton{font-weight:300;cursor:pointer;float:left;font-size:22px;height:38px;line-height:38px;text-align:center;transition:all .4s ease 0;width:30%}.listcart input.cart-plus-minus-box{float:left;line-height:29px;width:40%;text-align:center;border:none;-moz-appearance:textfield;background:rgba(255,255,255,0.9) none repeat scroll 0 0;color:#777;font-size:14px;height:38px;line-height:14px;padding:8px;transition:all .3s ease 0}.listcart li .promotion{background:#fff;padding:5px 0;width:345px;height:auto;margin:0 10px 0 0;display:block;overflow:hidden}.listcart li .promotion span.promo:before{content:'•';display:inline-block;vertical-align:middle;margin-right:5px;line-height:18px;font-size:20px;color:#999;margin:0 3px 0 -10px;float:left}.listcart .cartitem span.promo{display:block;overflow:hidden;padding-left:10px;color:#333;margin-bottom:5px;font-size:12px}.listcart .cartitem{float:left;width:100%;padding:10px 0;border-bottom:1px solid #eee;position:relative}.listcart .cartitem .oimg{width:20%;height:100%;position:relative;float:left;text-align:center}.listcart .cartitem .oimg img{max-height:100px}.listcart .cartitem .odel{color:#666;display:block;margin-top:5px;font-size:13px}.listcart .cartitem .odel:before{content:"\f00d";font:normal 11px/1 FontAwesome;margin-right:3px;background:#ccc;padding:2px;border-radius:50%;display:inline-block;width:14px;height:14px;color:#fff}.listcart .cartitem .odel:hover{color:#f00}.listcart .cartitem .oname{width:80%;float:left;margin-bottom:5px;padding-left:20px}.listcart .cartitem h3{float:left;width:70%;line-height:15px}.listcart .cartitem h3 a{overflow:hidden;display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;text-overflow:ellipsis;font:bold 14px/20px 'Roboto';color:#000}.listcart .cartitem label{color:#e00;font-size:15px;display:inline-block;width:30%;text-align:right}.mgg-code{color:#288ad6 !important;cursor:pointer;clear:both;text-align:right}.mgg-inputcode{display:none;overflow:hidden;margin:10px 0}.mgg-inputcode button{float:right;background:#288ad6;border-radius:4px;border:0;height:40px;font-size:14px;color:#fff;width:90px;text-align:center;cursor:pointer}.mgg-inputcode input{display:block;width:40%;border:1px solid #a6a6a6;border-radius:4px;padding:9px;height:40px;float:right;margin-right:10px}.title-user{text-transform:uppercase;border-top:6px solid #f0f0f0;padding:15px 0;font-weight:600;clear:both}.listcart .cartitem .promo label{text-align:left}.total{float:left;width:100%;margin:12px 0;font-size:14px}.total div{overflow:hidden;font-size:14px;color:#333;line-height:24px;position:relative}.total span{float:left}.total b,.total i{float:right;color:#333}.total b.total_money{float:right;font-size:16px;color:#e00}.bt-payment{background-color:#e23a24;border:0 none;border-radius:3px;color:#fff;font-size:16px;font-weight:bold;margin-top:10px;padding:10px 30px;text-align:center;text-transform:uppercase}.bt-payment:hover,.bt-payment:focus{background-color:#333d94}.radioPure{cursor:pointer;display:inline-block;font-size:14px;padding-right:20px}.radioPure:hover .inner{background-color:#e00;opacity:0.5;transform:scale(0.5)}.radioPure input{height:1px;opacity:0;width:1px}.radioPure input:checked + .outer .inner{opacity:1;transform:scale(1)}.radioPure input:checked + .outer{border:1px solid #e00}.radioPure input:focus + .outer .inner{background-color:#e00;opacity:1;transform:scale(1)}.radioPure .outer{background-color:#fff;border:1px solid #a6a6a6;border-radius:50%;display:block;float:left;height:18px;width:18px}.radioPure .inner{background-color:#e00;border-radius:50%;display:block;height:8px;margin:4px;opacity:0;transform:scale(0);transition:all 0.25s ease-in-out 0s;width:8px}.radioPure i{color:#333;display:inline-block;font-style:normal;font-weight:normal;padding-left:5px}.itemCheckBox{padding-right:30px;position:relative;margin:0;cursor:pointer}.itemCheckBox input[type="checkbox"]{height:0.1px;opacity:0;display:none;width:0.1px}.itemCheckBox span{padding-left:5px;position:relative;top:-4px;font-weight:600}.check-box{background-color:transparent;border:1px solid #a6a6a6;border-radius:2px;box-sizing:border-box;cursor:pointer;display:inline-block;width:18px;height:18px;position:relative;transition:border-color 0.2s ease 0s}.check-box input[type="checkbox"]{display:none}.check-box::before,.check-box::after{background-color:#e00;border-radius:5px;box-sizing:border-box;content:"";display:inline-block;height:0;position:absolute;transform-origin:left top 0;width:2px}.check-box::before{left:7px;top:14px;transform:rotate(-135deg)}.check-box::after{left:1px;top:8px;transform:rotate(-45deg)}input[type="checkbox"]:checked + .check-box,.check-box.checked{border-color:#e00}input[type="checkbox"]:checked + .check-box::after,.check-box.checked::after{animation:0.2s ease 0s normal forwards 1 running dothabottomcheck;height:7px}input[type="checkbox"]:checked + .check-box::before,.check-box.checked::before{animation:0.2s ease 0s normal forwards 1 running dothatopcheck;height:24px}.btn-blue{background-image:-o-radial-gradient(70px,#23da44,#23a948);background-image:radial-gradient(70px,#2994ef,#0067bf);color:#fff;padding:10px 30px;font-size:15px;display:inline-block;text-transform:uppercase;text-shadow:0 1px 0 rgba(0,0,0,.3)}.btn.w100{width:100%}.btn-blue:hover,.btn:focus,.btn.focus{text-decoration:none;color:#fff}.bgmanage-content{display:block;margin:20px 0;width:100%;padding:30px 20px 20px;background:#fff;border-radius:4px;-webkit-border-radius:4px;float:left}.title-manage{margin-top:15px;font-size:20px;font-family:'Roboto',sans-serif;font-weight:300;color:#242424}.profiles{background:0 0;color:#fff;padding:10px 5px 5px;font-family:'Roboto',sans-serif}.profiles .image{width:45px;height:45px;overflow:hidden;float:left;margin-right:10px;margin-bottom:0}.profiles .image img{border-radius:50%;-webkit-border-radius:50%}.profiles .name{font-size:13px;margin-bottom:5px;color:#242424;margin-top:2px;font-weight:300}.profiles h6{margin:0;font-size:16px;font-weight:400;color:#242424}.menu-list{padding-top:10px}.menu-list .item{}.menu-list .item a{white-space:nowrap;font-size:13px;font-weight:400;text-align:left;color:#4a4a4a;padding-top:10px;padding-bottom:10px;padding-left:60px;position:relative;display:block}.menu-list .item.active a,.menu-list .item:hover a{color:#000;text-decoration:none;background:#ececec;border-radius:2px;-webkit-border-radius:2px}.menu-list .item a i{font-size:18px;height:30px;text-align:center;width:40px;position:absolute;top:0;left:7px;bottom:0;margin:auto;line-height:30px;color:#999}.block-btn .btn{padding:10px 20px;margin-bottom:20px;transition:border-color .1s ease-in-out 0s,background-color .1s ease-in-out 0;background-color:#f4786e;border-bottom-color:#dd191d;font-size:13px;color:#fff}.clear{clear:both}.account-profile{clear:both}.account-profile .form-group{margin-bottom:15px;width:100%;float:left}.sp-buy{display:block;margin:20px 0;padding:1px;width:100%;background:#fff;border-radius:4px;-webkit-border-radius:4px;float:left}.sp-buy table{border-collapse:collapse;width:100%;word-break:break-word}.sp-buy table a{color:#007ff0;text-decoration:none}.sp-buy tr{border-bottom:1px solid #f4f4f4}.sp-buy tr:hover{background:#f8ffff}.sp-buy th,.sp-buy td{min-width:130px;color:#787878;font-size:15px;font-weight:400;text-align:left;padding:15px}.sp-buy td{font-size:13px}.heading-dh{font-size:19px;font-weight:300;font-family:Roboto;display:flex;-webkit-box-align:center;align-items:center;margin:20px 0 0}.heading-dh .split{margin:0 5px}.heading-dh .status{font-weight:bolder;color:#4a90e2}.knBZCc{display:flex;flex-direction:column;color:rgba(0,0,0,0.65);margin:10px 0 20px}.knBZCc .title{font-size:13px;text-transform:uppercase;color:#242424;margin:0 0 15px}.knBZCc .content{display:flex;background-color:#fff;height:100%;padding:10px;border-radius:4px}.knBZCc .content .notifications{display:flex;flex-direction:column}.knBZCc .content .notifications__item .date{min-width:160px}.knBZCc .content .notifications__item .comment{text-align:left}.knBZCc .content .notifications__item{display:flex}.bLftah{display:flex;margin:10px 0 20px}.bLftah > div:first-child{margin-left:0}.bLftah > div{width:33.3333%;margin:0 5px}.hZxPtg{display:flex;flex-direction:column;color:rgba(0,0,0,0.65);margin:10px 0 20px}.hZxPtg .title{font-size:13px;text-transform:uppercase;color:#242424;margin:0 0 15px}.hZxPtg .content{display:flex;flex-direction:column;background-color:#fff;height:100%;padding:10px;border-radius:4px}.bLftah .name{color:#242424;font-weight:700;text-transform:uppercase}.bLftah p{margin:5px 0 0}.kmKglE{width:100%;color:#424242;display:table;font-size:13px;border-collapse:collapse;line-height:1.5;word-break:break-word;background:#fff;border-radius:4px;border-spacing:0}.kmKglE tr{display:table-row;padding:10px}.kmKglE thead tr th:first-child{border-left:none}.kmKglE tbody tr{border-bottom:1px solid #f4f4f4}.kmKglE thead tr th{display:table-cell;min-width:100px;position:relative;color:#787878;font-size:15px;font-weight:400;text-align:left;padding:20px 15px;border-top:none;background:0 0;border-bottom:1px solid #f4f4f4}.kmKglE tbody tr td{position:relative;display:table-cell;color:#242424;vertical-align:top;min-width:100px;border-width:initial;border-style:none;border-color:initial;border-image:initial;padding:20px 15px}.kmKglE tbody tr td .product-item{display:flex}.kmKglE tbody tr td .product-item img{width:60px;height:60px;margin-right:10px}.kmKglE tbody tr td .product-item .product-info .product-name{font-size:14px;color:#242424}.kmKglE tfoot tr td{text-align:right;display:table-cell;max-width:550px;min-width:125px;color:#242424;padding:5px 20px}.kmKglE tfoot tr:last-child td{padding-bottom:30px}.kmKglE tfoot tr td span{color:#787878;font-size:14px}.kmKglE tfoot tr td .sum{color:#ff3b27;font-size:18px}.kmKglE tfoot tr:first-child td{padding-top:30px;border-top:none}.view-list-order{margin-bottom:30px;margin-top:15px;display:inline-block;margin-right:15px;cursor:pointer;border-width:1px;border-style:solid;border-color:transparent;border-image:initial}.created-date{display:flex;font-size:13px;align-items:flex-end;flex-direction:column}.form-list label{font-weight:400}.form-list .itemCheckBox span{font-weight:400;color:#888}.birthday-picker .form-control{width:30%}.form-list select[name="birth[month]"]{margin:0 5%}#password-group,#xhd-group{display:none;float:left;width:100%}.account-profile .form-list .col-md-3{padding-left:0}.sub_show{display:none;padding:15px;background:#f5f5f5;margin:10px 0 0 0}ul.listBankATM{margin:20px 0 0 0;clear:both}ul.listBankATM .img-bank{width:15%;display:inline-block;vertical-align:top}ul.listBankATM .detail-bank{width:80%;display:inline-block;padding-left:25px}.sub_header_hot .title{background:#f34723;display:block;float:left;width:100%;font-size:18px}.sub_header_hot a{color:white;display:block;float:left;width:100%;text-align:center;padding:10px 5px 5px 5px}.sub_header_hot{display:block;float:left;width:100%}li.brand-menu{width:50% !important}.footer-new-right h3{margin-bottom:0;font-size:14px;text-transform:unset}.footer-new-center{height:470px;color:black !important}.title-showroom{text-transform:uppercase;font-size:24px;padding:0;clear:both;text-align:center;width:100%;font-weight:bold;color:#ff4d07;padding-top:10px}.footer-new-left,.footer-new-center,.footer-new-right{position:relative;color:#f34723;max-height:470px;overflow-y:scroll}ul.index-brand li{height:250px !important}ul.index-brand li h3{text-align:center}ul.index-brand li h3 a{color:#f34723;text-transform:uppercase;font-weight:bold}.news-main{width:calc(100% - 280px) !important;width:-webkit-calc(100% - 280px) !important;padding:0 10px 0 0}h3.title-news-menu{background:#fd746c;background:-webkit-linear-gradient(to right,#ff9068,#fd746c);background:linear-gradient(to right,#ff9068,#fd746c);color:white;padding:5px 10px;font-size:18px}h3.title-newest-news{background:#6a3093;background:-webkit-linear-gradient(to right,#a044ff,#6a3093);background:linear-gradient(to right,#a044ff,#6a3093);color:white;padding:5px 10px;font-size:18px}h3.title-hot-news{background:#C02425;background:-webkit-linear-gradient(to right,#F0CB35,#C02425);background:linear-gradient(to right,#F0CB35,#C02425);padding:5px 10px;font-size:18px;color:white}ul.list-news-menu li{padding:5px 30px;border-bottom:1px solid #ccc;color:black}ul.list-news-menu li a{color:black}ul.list-news-menu li:hover{background:#f34723}ul.list-news-menu li:hover a{color:white}.news-menu,.newest-news,.hot-news{margin-bottom:15px;border-radius:6px;background:#fff;display:block;float:left;width:100%}.cate_pro_title{text-align:center}#mapnopop iframe{width:100%;height:280px}.loading{background:#000;opacity:0.5;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:9999;display:none}.loading .icon{background:url("/Content/desktop/css/images/loading.gif") no-repeat center center / 50px 50px;position:absolute;left:0;right:0;top:0;bottom:0;margin:auto;width:50px;height:50px;text-indent:-9999px}.pagination{text-align:center}button.bk-btn-installment{width:100%;margin-top:10px}</style>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PZCF476');</script>
    <!-- End Google Tag Manager -->

    <meta name="google-site-verification" content="C7PCnlclWOEKoNdat4EFQf52CLeu7QP7OE1RQcH8ozM" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146658488-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146658488-1');
    </script>


    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-960523436"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-960523436');
        gtag('config', 'AW-762876081');
        gtag('config', 'AW-729715783');
        gtag('config', 'AW-873598397');

    </script>

    <script>
        gtag('event', 'page_view', {
            'send_to': 'AW-762876081',
            'value': 'replace with value',
            'items': [{
                'id': 'replace with value',
                'google_business_vertical': 'retail'
            }]
        });
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '222265539728554');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=222265539728554&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '353967766344134');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=353967766344134&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body id="body">



<div class="Wrapper">
    <header>


        <div class="er-banner-top">
            <a class="banner-link" href="#" title="banner-top">
                <img src="/Content/desktop/images/1920x65_Gif.gif" alt=""></a><span class="close_top_banner">✕</span>
        </div>

        <div class="header-wrap">
            <div class="container">
                <div class="row">

                    <div class="logo">

                        <a href="/" title="BếpTốt.vn">
                            <img width="120" height="60" src="/Data/upload/files/logg_beptotv2(1).png" alt="Bếp Tốt.vn" />
                        </a>
                    </div>




                    <div class="search-boxtr">
                        <form class="headsearch" method="post" id="formSearchTop" onsubmit="doSearch(); return false;">
                            <input type="text" name="keyword" id="keyword" onkeyup="search(this.value)" autocomplete="off" value="" placeholder="Nhập từ khóa tìm kiếm" class="topinput">
                            <button type="submit" class="btn btntop"><i class="fa fa-search"></i></button>
                            <ul class="resuiltSearch ul-menu-muiten .search-suggest"></ul>
                        </form>
                    </div>
                    <div class="nav-user-txt">

                        <li class="">
                            <a href="/tin-tuc.html">
                                <div class="icon"><i class="akr-Menuicon_Sale"></i></div>
                                <span class="user">Tin tức</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="/dich-vu.html">
                                <div class="icon"><i class="akr-icon_Warranty1"></i></div>
                                <span class="user">Dịch vụ</span>
                            </a>
                        </li>
                        <li class="sp-view">
                            <a class="btnviewed" href="/khuyen-mai.html">
                                <span>Khuyến mãi</span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:0986083083">
                                <div class="icon"><i class="akr-Contact_PhoneCall"></i></div>
                                <p><span>0986.083.083</span></p>
                            </a>
                        </li>
                        <li><a href="https://beptot.vn/gio-hang.html">
                                <div class="icon"><i class="akr-Headericon_Cart"></i><span class="mount">0</span></div>
                                <span>Giỏ hàng</span></a></li>
                    </div>

                    <script type="text/javascript">
                        function doSearch() {
                            var keyword = $('#keyword').val();

                            if (keyword.length < 2 || keyword == 'Nhập từ khóa tìm kiếm') {
                                zebra_alert('Thông báo !', 'Từ khóa phải nhiều hơn 1 ký tự.');
                                return;
                            }
                            location.href = 'https://beptot.vn/tim-kiem.html?keyword=' + nonAccentVietnamese(keyword);
                        }
                        function nonAccentVietnamese(str) {
                            str = str.toLowerCase();
                            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                            str = str.replace(/đ/g, "d");
                            // Some system encode vietnamese combining accent as individual utf-8 characters
                            str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
                            str = str.replace(/\u02C6|\u0306|\u031B/g, "");
                            str = str.replace(/\s/g, '-');// Â, Ê, Ă, Ơ, Ư
                            return str;
                        }
                    </script>

                </div>
            </div>
        </div>

        <nav class="menu_main_cate">
            <div class="container">
                <div class="row">
                    <ul class="menu-right-head">

                        <li class="level1">
                            <a href="https://beptot.vn/bep-tu.html" title="Bếp từ">
                                <i class="icon-menu mn-bep-tu"></i>
                                <span>Bếp từ</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li class="brand-menu">
                                    <a title="Bếp từ Bosch" href="https://beptot.vn/bep-tu-bosch.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/logo-boschx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Fagor" href="https://beptot.vn/bep-tu-fagor.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/fagorx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Eurosun" href="https://beptot.vn/bep-tu-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="BẾP TỪ SPELIER" href="https://beptot.vn/bep-tu-spelier.html">
                                        <img src="/Data/ResizeImage/images/unnamed-2-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Nagakawa" href="https://beptot.vn/bep-tu-nagakawa.html">
                                        <img src="/Data/ResizeImage/files/logo-ko-rx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Chefs" href="https://beptot.vn/bep-tu-chefs.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/chefsx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Hafele" href="https://beptot.vn/bep-tu-hafele.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Hafelex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Siemens" href="https://beptot.vn/bep-tu-siemens.html">
                                        <img src="/Data/ResizeImage/files/adv/logo-thuonghieu/siemensx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Canzy" href="https://beptot.vn/bep-tu-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp từ Electrolux" href="https://beptot.vn/bep-tu-electrolux.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/electroluxx100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/bep-dien-tu.html" title="Bếp điện từ">
                                <i class="icon-menu mn-bep-dien-tu"></i>
                                <span>Bếp điện từ</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Bosch" href="https://beptot.vn/bep-dien-tu-bosch.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/logo-boschx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Eurosun" href="https://beptot.vn/bep-dien-tu-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="BẾP ĐIỆN TỪ SPELIER" href="https://beptot.vn/bep-dien-tu-spelier.html">
                                        <img src="/Data/ResizeImage/images/unnamed-2-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Nagakawa" href="https://beptot.vn/bep-dien-tu-nagakawa.html">
                                        <img src="/Data/ResizeImage/files/logo-ko-rx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Chefs" href="https://beptot.vn/bep-dien-tu-chefs.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/chefsx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Hafele" href="https://beptot.vn/bep-dien-tu-hafele.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Hafelex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Canzy" href="https://beptot.vn/bep-dien-tu-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Cata" href="https://beptot.vn/bep-dien-tu-cata.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Catax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ D'mestik" href="https://beptot.vn/bep-dien-tu-dmestik.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Dmestikx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện từ Malloca" href="https://beptot.vn/bep-dien-tu-malloca.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/mallocax100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/bep-dien.html" title="Bếp điện">
                                <i class="icon-menu mn-bep-dien"></i>
                                <span>Bếp điện</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li class="brand-menu">
                                    <a title="Bếp điện Bosch" href="https://beptot.vn/bep-dien-bosch.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/logo-boschx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Fagor" href="https://beptot.vn/bep-dien-fagor.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/fagorx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Eurosun" href="https://beptot.vn/bep-dien-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Chefs" href="https://beptot.vn/bep-dien-chefs.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/chefsx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Hafele" href="https://beptot.vn/bep-dien-hafele.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Hafelex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Teka" href="https://beptot.vn/bep-dien-teka.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/tekax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Canzy" href="https://beptot.vn/bep-dien-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Electrolux" href="https://beptot.vn/bep-dien-electrolux.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/electroluxx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Malloca" href="https://beptot.vn/bep-dien-malloca.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/mallocax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp điện Cata" href="https://beptot.vn/bep-dien-cata.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Catax100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/bep-gas.html" title="Bếp gas">
                                <i class="icon-menu mn-bep-gas"></i>
                                <span>Bếp gas</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li><a title="Bếp gas âm" href="https://beptot.vn/bep-gas-am.html">Bếp gas âm</a></li>

                                <li><a title="Bếp gas dương" href="https://beptot.vn/bep-gas-duong.html">Bếp gas dương</a></li>

                                <li><a title="Bếp gas âm hồng ngoại" href="https://beptot.vn/bep-gas-am-hong-ngoai.html">Bếp gas âm hồng ngoại</a></li>

                                <li><a title="Bếp gas dương hồng ngoại" href="https://beptot.vn/bep-gas-duong-hong-ngoai.html">Bếp gas dương hồng ngoại</a></li>

                                <li><a title="Bếp gas giá rẻ" href="https://beptot.vn/bep-gas-gia-re.html">Bếp gas giá rẻ</a></li>

                                <li><a title="Bếp gas công nghiệp| Bếp gas nhà hàng, khách sạn" href="https://beptot.vn/bep-gas-cong-nghiep.html">Bếp gas công nghiệp| Bếp gas nhà hàng, khách sạn</a></li>

                                <li><a title="Bếp gas kết hợp điện từ" href="https://beptot.vn/bep-gas-ket-hop-dien-tu.html">Bếp gas kết hợp điện từ</a></li>

                                <li><a title="Bếp và bình gas" href="https://beptot.vn/bo-binh-bep-gas.html">Bếp và bình gas</a></li>

                                <li><a title="Bếp gas đơn" href="https://beptot.vn/bep-gas-don.html">Bếp gas đơn</a></li>

                                <li><a title="Bếp gas du lịch" href="https://beptot.vn/bep-gas-du-lich.html">Bếp gas du lịch</a></li>

                                <li><a title="Bình gas & Linh kiện" href="https://beptot.vn/binh-gas-linh-kien.html">Bình gas & Linh kiện</a></li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Taka" href="https://beptot.vn/bep-gas-taka.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Eurosun" href="https://beptot.vn/bep-gas-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Sunhouse" href="https://beptot.vn/bep-gas-sunhouse.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/sunhousex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp Gas Rinnai" href="https://beptot.vn/bep-gas-rinnai.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas paloma" href="https://beptot.vn/bep-gas-paloma.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/palomax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Binova" href="https://beptot.vn/bep-gas-binova.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Binovax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Teka" href="https://beptot.vn/bep-gas-teka.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/tekax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Canzy" href="https://beptot.vn/bep-gas-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Lorca" href="https://beptot.vn/bep-gas-lorca.html">
                                        <img src="/Data/ResizeImage/files/adv/logo-thuonghieu/lorcax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Bếp gas Faber" href="https://beptot.vn/bep-gas-faber.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/faberx100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/may-hut-mui.html" title="Máy hút mùi">
                                <i class="icon-menu mn-may-hut-mui"></i>
                                <span>Máy hút mùi</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li><a title="Máy hút mùi ống khói" href="https://beptot.vn/may-hut-mui-ong-khoi.html">Máy hút mùi ống khói</a></li>

                                <li><a title="Máy hút mùi cổ điển" href="https://beptot.vn/may-hut-mui-co-dien.html">Máy hút mùi cổ điển</a></li>

                                <li><a title="Máy hút mùi âm tủ" href="https://beptot.vn/may-hut-mui-am-tu.html">Máy hút mùi âm tủ</a></li>

                                <li><a title="Máy hút mùi đảo" href="https://beptot.vn/may-hut-mui-dao.html">Máy hút mùi đảo</a></li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Bosch" href="https://beptot.vn/may-hut-mui-bosch.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/logo-boschx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Fagor" href="https://beptot.vn/may-hut-mui-fagor.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/fagorx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Eurosun" href="https://beptot.vn/may-hut-mui-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="MÁY HÚT MÙI SPELIER" href="https://beptot.vn/may-hut-mui-spelier.html">
                                        <img src="/Data/ResizeImage/images/unnamed-2-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy Hút Mùi Nagakawa" href="https://beptot.vn/may-hut-mui-nagakawa.html">
                                        <img src="/Data/ResizeImage/files/logo-ko-rx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Chefs" href="https://beptot.vn/may-hut-mui-chefs.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/chefsx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Hafele" href="https://beptot.vn/may-hut-mui-hafele.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Hafelex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Taka" href="https://beptot.vn/may-hut-mui-taka.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi Electrolux" href="https://beptot.vn/may-hut-mui-electrolux.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/electroluxx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy hút mùi D'mestik" href="https://beptot.vn/may-hut-mui-dmestik.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Dmestikx100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/lo-nuong-da-nang.html" title="Lò nướng đa năng">
                                <i class="icon-menu mn-lo-nuong-da-nang"></i>
                                <span>Lò nướng đa năng</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li class="brand-menu">
                                    <a title="Lò nướng Bosch" href="https://beptot.vn/lo-nuong-bosch.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/logo-boschx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Eurosun" href="https://beptot.vn/lo-nuong-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Spelier" href="https://beptot.vn/lo-nuong-spelier.html">
                                        <img src="/Data/ResizeImage/images/unnamed-2-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Malloca" href="https://beptot.vn/lo-nuong-malloca.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/mallocax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Chefs" href="https://beptot.vn/lo-nuong-chefs.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/chefsx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Hafele" href="https://beptot.vn/lo-nuong-hafele.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Hafelex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Elica" href="https://beptot.vn/lo-nuong-elica.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Elicax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Canzy" href="https://beptot.vn/lo-nuong-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Electrolux" href="https://beptot.vn/lo-nuong-electrolux.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/electroluxx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Lò nướng Cata" href="https://beptot.vn/lo-nuong-cata.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Catax100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/may-say-chen-bat.html" title="Máy sấy chén bát">
                                <i class="icon-menu mn-may-say-chen-bat"></i>
                                <span>Máy sấy chén bát</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li><a title="Máy sấy chén bát âm tủ" href="https://beptot.vn/may-say-chen-bat-am-tu.html">Máy sấy chén bát âm tủ</a></li>

                                <li><a title="Máy sấy chén bát treo tường" href="https://beptot.vn/may-say-chen-bat-treo-tuong.html">Máy sấy chén bát treo tường</a></li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Binova" href="https://beptot.vn/may-say-bat-binova.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Binovax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Canzy" href="https://beptot.vn/may-say-bat-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Dudoff" href="https://beptot.vn/may-say-bat-dudoff.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Dudoffx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Electrolux" href="https://beptot.vn/may-say-bat-electrolux.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/electroluxx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Faster" href="https://beptot.vn/may-say-bat-faster.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/fasterx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Giovani" href="https://beptot.vn/may-say-bat-giovani.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Giovanix100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Malloca" href="https://beptot.vn/may-say-bat-malloca.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/mallocax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Napoliz" href="https://beptot.vn/may-say-bat-napoliz.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/napolizx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Sevilla" href="https://beptot.vn/may-say-bat-sevilla.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Evillax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy sấy bát Taka" href="https://beptot.vn/may-say-bat-taka.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="https://beptot.vn/may-rua-chen-bat.html" title="Máy rửa chén bát">
                                <i class="icon-menu mn-may-rua-chen-bat"></i>
                                <span>Máy rửa chén bát</span>

                                <span class="angle-down"></span>

                            </a>

                            <ul class="dropdown-listchildpage">

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Bosch" href="https://beptot.vn/may-rua-bat-bosch.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/logo-boschx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Eurosun" href="https://beptot.vn/may-rua-bat-eurosun.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="MÁY RỬA BÁT SPELIER" href="https://beptot.vn/may-rua-bat-spelier.html">
                                        <img src="/Data/ResizeImage/images/unnamed-2-x100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Malloca" href="https://beptot.vn/may-rua-bat-malloca.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/mallocax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Chef's" href="https://beptot.vn/may-rua-bat-chefs.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/chefsx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Hafele" href="https://beptot.vn/may-rua-bat-hafele.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Hafelex100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Teka" href="https://beptot.vn/may-rua-bat-teka.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/tekax100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Canzy" href="https://beptot.vn/may-rua-bat-canzy.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Fagor" href="https://beptot.vn/may-rua-bat-fagor.html">
                                        <img src="/Data/ResizeImage/files/banner/logo hang sx/fagorx100x50x4.webp" />
                                    </a>
                                </li>

                                <li class="brand-menu">
                                    <a title="Máy rửa bát Cata" href="https://beptot.vn/may-rua-bat-cata.html">
                                        <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Catax100x50x4.webp" />
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="level1">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-menu mn-thiet-bi-khac"></i>
                                <span>Thiết bị nhà bếp khác</span>
                                <span class="angle-down"></span>
                            </a>
                            <ul class="dropdown-listchildpage">

                                <li><a title="Chậu rửa chén bát" href="https://beptot.vn/chau-voi-rua-chen-bat.html">Chậu rửa chén bát</a></li>

                                <li><a title="Vòi rửa chén bát" href="https://beptot.vn/voi-rua-chen-bat.html">Vòi rửa chén bát</a></li>

                                <li><a title="Phụ kiện chậu vòi" href="https://beptot.vn/phu-kien-chau-voi.html">Phụ kiện chậu vòi</a></li>

                                <li><a title="Tủ lạnh - Tủ Rượu" href="https://beptot.vn/tu-lanh-tu-ruou.html">Tủ lạnh - Tủ Rượu</a></li>

                                <li><a title="Máy lọc nước" href="https://beptot.vn/may-loc-nuoc.html">Máy lọc nước</a></li>

                                <li><a title="Cây nước nóng lạnh" href="https://beptot.vn/cay-nuoc-nong-lanh.html">Cây nước nóng lạnh</a></li>

                                <li><a title="Bình Nóng Lạnh" href="https://beptot.vn/binh-nong-lanh.html">Bình Nóng Lạnh</a></li>

                                <li><a title="Lò vi sóng" href="https://beptot.vn/lo-vi-song.html">Lò vi sóng</a></li>

                                <li><a title="Máy lọc không khí" href="https://beptot.vn/may-loc-khong-khi.html">Máy lọc không khí</a></li>

                                <li><a title="Quạt điều hòa" href="https://beptot.vn/quat-dieu-hoa.html">Quạt điều hòa</a></li>

                                <li><a title="Đồ gia dụng" href="https://beptot.vn/do-gia-dung.html">Đồ gia dụng</a></li>

                                <li><a title="Tủ Bếp" href="https://beptot.vn/tu-bep.html">Tủ Bếp</a></li>

                                <li><a title="Phụ kiện nhà tắm" href="https://beptot.vn/phu-kien-nha-tam.html">Phụ kiện nhà tắm</a></li>

                                <li><a title="Phụ kiện nhà bếp" href="https://beptot.vn/phu-kien-nha-bep.html">Phụ kiện nhà bếp</a></li>

                                <li><a title="Viên rửa bát" href="https://beptot.vn/vien-rua-bat.html">Viên rửa bát</a></li>

                                <li><a title="Nồi chiên không dầu" href="https://beptot.vn/noi-chien-khong-dau.html">Nồi chiên không dầu</a></li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="overlay"></div>

    </header>
    <div class="content-main">
        <div class="container">
            <div class="row">


                <ul class="breadcrumb" itemprop="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
                    <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/" itemprop="url"><span itemprop="title">beptot.vn</span></a>
                    </li>   <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="https://beptot.vn/bep-gas.html" itemprop="url"><span itemprop="title">Bếp gas</span></a>
                    </li>


                </ul>


                <div class="banner-cate owl-carousel owl-theme">

                    <div class="item">
                        <a href="https://beptot.vn/may-rua-chen-bat.html" title="máy rửa bát">
                            <img class="owl-lazy" data-src="/Data/ResizeImage/files/banner/3-1-x400x150x4.webp" alt="máy rửa bát" />
                        </a>
                    </div>

                    <div class="item">
                        <a href="https://beptot.vn/tu-bep.html" title="Tủ bếp">
                            <img class="owl-lazy" data-src="/Data/ResizeImage/files/banner/1-1-x400x150x4.webp" alt="Tủ bếp" />
                        </a>
                    </div>

                    <div class="item">
                        <a href="https://beptot.vn/bep-tu.html" title="Bếp từ">
                            <img class="owl-lazy" data-src="/Data/ResizeImage/files/banner/2-1-x400x150x4.webp" alt="Bếp từ" />
                        </a>
                    </div>

                </div>


                <div class="hd-card-body top-page-content">

                    <h1><a href="https://beptot.vn/bep-gas.html" class="fs-hotit" title="Bếp gas sự lựa chọn thông minh của mọi gia đình!">Bếp gas sự lựa chọn thông minh của mọi gia đình!</a></h1>

                    <section class="descriptionTopCtgr">
                        <div class="noteDescription"><p>Bếp gas là một vật dụng đã trở nên quá quen thuộc trong mọi căn bếp Việt. Hiện nay, bếp gas không chỉ có mỗi bếp gas dương cổ điển mà còn có bếp gas âm, bếp gas hồng ngoại, bếp gas dương hồng ngoại,...Bếp gas nói chung đều có thiết kế hiện đại, mẫu mã đa dạng, tính thẩm mỹ cao và đặc biệt có độ an toàn cao. Nếu bạn đang có ý định mua bếp gas thì không thể bỏ lỡ các mẫu mã nói chung và các hãng nói riêng mà chúng tôi cung cấp dưới đây để bạn có sự lựa chọn tốt nhất cho gia đình mình nhé!</p>
                        </div>
                    </section>

                    <div class="hd-module-title filterBoxFixed">
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="boxFilterLeft btn-group">
                                    <button type="button" class="btn btn-filters btn-default dropdown-toggle" data-toggle="dropdown">
                                        Phân loại <span class="caret"></span>
                                    </button>
                                    <ul class="listform_filter category right-property dropdown-menu" role="menu">

                                        <li data-url="https://beptot.vn/bep-gas-am.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas âm</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-duong.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas dương</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-am-hong-ngoai.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas âm hồng ngoại</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-duong-hong-ngoai.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas dương hồng ngoại</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-gia-re.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas giá rẻ</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-cong-nghiep.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas công nghiệp| Bếp gas nhà hàng, khách sạn</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-ket-hop-dien-tu.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas kết hợp điện từ</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bo-binh-bep-gas.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp và bình gas</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-don.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas đơn</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-du-lich.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bếp gas du lịch</label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/binh-gas-linh-kien.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>Bình gas & Linh kiện</label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>

                                <div class="boxFilterLeft btn-group">
                                    <button type="button" class="btn btn-filters btn-default dropdown-toggle" data-toggle="dropdown">
                                        Hãng Sản xuất <span class="caret"></span>
                                    </button>
                                    <ul class="listform_filter category right-property dropdown-menu" role="menu">

                                        <li data-url="https://beptot.vn/bep-gas-eurosun.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/eurosun(1).jpg" class="icImgBrand" alt="Eurosun" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-taka.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/1-logo-thuong-hieu-0306/taka.png" class="icImgBrand" alt="Taka" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-sunhouse.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/1-logo-thuong-hieu-0306/sunhouse.png" class="icImgBrand" alt="Sunhouse" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-rinnai.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/1-logo-thuong-hieu-0306/Rinnai.png" class="icImgBrand" alt="Rinnai" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-paloma.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/paloma.jpg" class="icImgBrand" alt="Paloma" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-binova.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/1-logo-thuong-hieu-0306/Binova.png" class="icImgBrand" alt="Binova" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-teka.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/teka.jpg" class="icImgBrand" alt="Teka" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-lorca.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/logo-thuonghieu/lorca.jpg" class="icImgBrand" alt="Lorca" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-canzy.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/canzy.jpg" class="icImgBrand" alt="Canzy" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-faber.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/faber.jpg" class="icImgBrand" alt="Faber" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-redsun.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/redsun.jpg" class="icImgBrand" alt="Redsun" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-dynamic.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/dynamic(2).jpg" class="icImgBrand" alt="Dynamic" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-blueger.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/logo-thuonghieu/logo-blueger-1024x354.png" class="icImgBrand" alt="Blueger" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-namilux.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/Adv/1-logo-thuong-hieu-0306/Namilux.png" class="icImgBrand" alt="Namilux" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-fujipan.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/fujipan.jpg" class="icImgBrand" alt="Fujipan" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-vanessa.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/vanessa.jpg" class="icImgBrand" alt="Vanessa" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-faster.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/faster.jpg" class="icImgBrand" alt="Faster" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-napoliz.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/napoliz.jpg" class="icImgBrand" alt="Napoliz" /></label>
                                            </div>
                                        </li>

                                        <li data-url="https://beptot.vn/bep-gas-abbaka.html">
                                            <div class="checkbox">
                                                <input type="checkbox"  />
                                                <label>
                                                    <img src="/Data/upload/files/banner/logo%20hang%20SX/abbaka.jpg" class="icImgBrand" alt="Abbaka" /></label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>

                                <div class="boxFilterLeft btn-group">
                                    <button type="button" class="btn btn-filters btn-default dropdown-toggle" data-toggle="dropdown">
                                        Xuất xứ <span class="caret"></span>
                                    </button>
                                    <ul class="listform_filter right-property dropdown-menu" role="menu">

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu106" data-url="https://beptot.vn/bep-gas.html"  value="106" />
                                                <label for="xuat-xu106">Spain</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu107" data-url="https://beptot.vn/bep-gas.html"  value="107" />
                                                <label for="xuat-xu107">Germany</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu108" data-url="https://beptot.vn/bep-gas.html"  value="108" />
                                                <label for="xuat-xu108">EU</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu109" data-url="https://beptot.vn/bep-gas.html"  value="109" />
                                                <label for="xuat-xu109">China (PRC)</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu110" data-url="https://beptot.vn/bep-gas.html"  value="110" />
                                                <label for="xuat-xu110">Việt Nam</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu560" data-url="https://beptot.vn/bep-gas.html"  value="560" />
                                                <label for="xuat-xu560">Italy</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu561" data-url="https://beptot.vn/bep-gas.html"  value="561" />
                                                <label for="xuat-xu561">Indonesia</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu562" data-url="https://beptot.vn/bep-gas.html"  value="562" />
                                                <label for="xuat-xu562">Malaysia</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu629" data-url="https://beptot.vn/bep-gas.html"  value="629" />
                                                <label for="xuat-xu629">Japan</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu630" data-url="https://beptot.vn/bep-gas.html"  value="630" />
                                                <label for="xuat-xu630">Korea</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu663" data-url="https://beptot.vn/bep-gas.html"  value="663" />
                                                <label for="xuat-xu663">France</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu707" data-url="https://beptot.vn/bep-gas.html"  value="707" />
                                                <label for="xuat-xu707">Thailand</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu825" data-url="https://beptot.vn/bep-gas.html"  value="825" />
                                                <label for="xuat-xu825">Redsun Chính hãng</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu826" data-url="https://beptot.vn/bep-gas.html"  value="826" />
                                                <label for="xuat-xu826">Blueger chính hãng</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu827" data-url="https://beptot.vn/bep-gas.html"  value="827" />
                                                <label for="xuat-xu827">Fujipan chính hãng </label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu830" data-url="https://beptot.vn/bep-gas.html"  value="830" />
                                                <label for="xuat-xu830">Thụy Điển</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu831" data-url="https://beptot.vn/bep-gas.html"  value="831" />
                                                <label for="xuat-xu831">Poland</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu832" data-url="https://beptot.vn/bep-gas.html"  value="832" />
                                                <label for="xuat-xu832">England</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu837" data-url="https://beptot.vn/bep-gas.html"  value="837" />
                                                <label for="xuat-xu837">Đài Loan</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="xuat-xu" id="xuat-xu840" data-url="https://beptot.vn/bep-gas.html"  value="840" />
                                                <label for="xuat-xu840">Thái Lan</label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>

                                <div class="boxFilterLeft btn-group">
                                    <button type="button" class="btn btn-filters btn-default dropdown-toggle" data-toggle="dropdown">
                                        Giá <span class="caret"></span>
                                    </button>
                                    <ul class="listform_filter right-property dropdown-menu" role="menu">

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="gia" id="gia20" data-url="https://beptot.vn/bep-gas.html"  value="20" />
                                                <label for="gia20">Dưới 3 triệu</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="gia" id="gia21" data-url="https://beptot.vn/bep-gas.html"  value="21" />
                                                <label for="gia21"> 3 triệu - 5 triệu</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="gia" id="gia22" data-url="https://beptot.vn/bep-gas.html"  value="22" />
                                                <label for="gia22"> 5 triệu - 7 triệu</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="gia" id="gia23" data-url="https://beptot.vn/bep-gas.html"  value="23" />
                                                <label for="gia23"> 7 triệu - 10 triệu</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="gia" id="gia93" data-url="https://beptot.vn/bep-gas.html"  value="93" />
                                                <label for="gia93">Trên 10 triệu</label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>

                                <div class="boxFilterLeft btn-group">
                                    <button type="button" class="btn btn-filters btn-default dropdown-toggle" data-toggle="dropdown">
                                        Số bếp <span class="caret"></span>
                                    </button>
                                    <ul class="listform_filter right-property dropdown-menu" role="menu">

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="so-bep" id="so-bep822" data-url="https://beptot.vn/bep-gas.html"  value="822" />
                                                <label for="so-bep822">1 bếp</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="so-bep" id="so-bep823" data-url="https://beptot.vn/bep-gas.html"  value="823" />
                                                <label for="so-bep823">2 bếp</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="so-bep" id="so-bep824" data-url="https://beptot.vn/bep-gas.html"  value="824" />
                                                <label for="so-bep824">3 bếp</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="so-bep" id="so-bep833" data-url="https://beptot.vn/bep-gas.html"  value="833" />
                                                <label for="so-bep833">4 bếp</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="so-bep" id="so-bep834" data-url="https://beptot.vn/bep-gas.html"  value="834" />
                                                <label for="so-bep834">5 bếp</label>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="checkbox">
                                                <input type="checkbox" name="so-bep" id="so-bep835" data-url="https://beptot.vn/bep-gas.html"  value="835" />
                                                <label for="so-bep835">6 bếp</label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>

                                <div class="boxFilterLeft btn-group">
                                    <button type="button" class="btn btn-filters btn-default dropdown-toggle" data-toggle="dropdown">
                                        Sắp xếp theo <span class="caret"></span>
                                    </button>
                                    <ul class="listform_filter right-property dropdown-menu filterTopCtgr" role="menu">
                                        <li>
                                            <div class="checkbox">
                                                <input data-url="https://beptot.vn/bep-gas.html" id="new_asc" name="sort" type="checkbox"  value="new_asc" />
                                                <label for="new_asc">Sản phẩm mới</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input data-url="https://beptot.vn/bep-gas.html" id="view_desc" name="sort" type="checkbox"  value="view_desc" />
                                                <label for="view_desc">Xem nhiều nhất</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input data-url="https://beptot.vn/bep-gas.html" id="price_asc" name="sort" type="checkbox"  value="price_asc" />
                                                <label for="price_asc">Giá thấp đến cao</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input data-url="https://beptot.vn/bep-gas.html" id="price_desc" name="sort" type="checkbox"  value="price_desc" />
                                                <label for="price_desc">Giá cao xuống thấp</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-am.html" title="Bếp gas âm">Bếp gas âm</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bep-gas-am-rinnai.html" title="Bếp gas âm Rinnai">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix80x40x4.webp" />
                            Bếp gas âm Rinnai
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-am-lorca.html" title="Bếp gas âm Lorca">

                            <img src="/Data/ResizeImage/files/adv/logo-thuonghieu/lorcax80x40x4.webp" />
                            Bếp gas âm Lorca
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-am-canzy.html" title="Bếp gas âm Canzy">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/canzyx80x40x4.webp" />
                            Bếp gas âm Canzy
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-am-binova.html" title="Bếp gas âm Binova">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Binovax80x40x4.webp" />
                            Bếp gas âm Binova
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-am-eurosun.html" title="Bếp gas âm Eurosun">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x80x40x4.webp" />
                            Bếp gas âm Eurosun
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-am.html" title="Xem tất cả Bếp gas âm">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-rinnai-rvb-212bg.html" title="Bếp gas âm Rinnai RVB-212BG">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-rinnai-rvb-212bgx500x500x4.webp" alt="Bếp gas âm Rinnai RVB-212BG" title="Bếp gas âm Rinnai RVB-212BG">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-rinnai-rvb-212bg.html" title="Bếp gas âm Rinnai RVB-212BG">Bếp gas âm Rinnai RVB-212BG</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">3.170.000₫</p>

                                <p>
                                    <span>4.550.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-30%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-rinnai-rvb-2gi-b.html" title="Bếp gas âm Rinnai RVB-2Gi(B)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-rinnai-rvb-2gi-bx500x500x4.webp" alt="Bếp gas âm Rinnai RVB-2Gi(B)" title="Bếp gas âm Rinnai RVB-2Gi(B)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-rinnai-rvb-2gi-b.html" title="Bếp gas âm Rinnai RVB-2Gi(B)">Bếp gas âm Rinnai RVB-2Gi(B)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">4.150.000₫</p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-rinnai-rvb-312bg.html" title="Bếp gas âm Rinnai RVB-312BG">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-rinnai-rvb-312bgx500x500x4.webp" alt="Bếp gas âm Rinnai RVB-312BG" title="Bếp gas âm Rinnai RVB-312BG">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-rinnai-rvb-312bg.html" title="Bếp gas âm Rinnai RVB-312BG">Bếp gas âm Rinnai RVB-312BG</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">3.780.000₫</p>

                                <p>
                                    <span>4.550.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-16%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-rinnai-rvb-2g-i-bw.html" title="Bếp gas âm Rinnai RVB-2G-i(BW)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-rinnai-rvb-2g-i-bwx500x500x4.webp" alt="Bếp gas âm Rinnai RVB-2G-i(BW)" title="Bếp gas âm Rinnai RVB-2G-i(BW)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-rinnai-rvb-2g-i-bw.html" title="Bếp gas âm Rinnai RVB-2G-i(BW)">Bếp gas âm Rinnai RVB-2G-i(BW)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">3.740.000₫</p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-rinnai-rvb-3hg-b.html" title="BẾP GAS ÂM RINNAI RVB-3HG (B)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-rinnai-rvb-3hg-bx500x500x4.webp" alt="BẾP GAS ÂM RINNAI RVB-3HG (B)" title="BẾP GAS ÂM RINNAI RVB-3HG (B)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-rinnai-rvb-3hg-b.html" title="BẾP GAS ÂM RINNAI RVB-3HG (B)">BẾP GAS ÂM RINNAI RVB-3HG (B)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">3.750.000₫</p>

                                <p>
                                    <span>5.562.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-32%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-rinnai-rvb-2hg-b.html" title="Bếp gas âm Rinnai RVB-2HG(B)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-rinnai-rvb-2hg-bx500x500x4.webp" alt="Bếp gas âm Rinnai RVB-2HG(B)" title="Bếp gas âm Rinnai RVB-2HG(B)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-rinnai-rvb-2hg-b.html" title="Bếp gas âm Rinnai RVB-2HG(B)">Bếp gas âm Rinnai RVB-2HG(B)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">3.300.000₫</p>

                                <p>
                                    <span>4.100.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-19%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-duong.html" title="Bếp gas dương">Bếp gas dương</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bep-gas-duong-rinnai.html" title="Bếp gas dương Rinnai">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix80x40x4.webp" />
                            Bếp gas dương Rinnai
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-duong-eurosun.html" title="Bếp gas dương Eurosun">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/eurosun-1-x80x40x4.webp" />
                            Bếp gas dương Eurosun
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-duong-dynamic.html" title="Bếp gas dương Dynamic">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/dynamic-2-x80x40x4.webp" />
                            Bếp gas dương Dynamic
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-duong-paloma.html" title="Bếp gas dương Paloma">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/palomax80x40x4.webp" />
                            Bếp gas dương Paloma
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-duong-sunhouse.html" title="Bếp gas dương Sunhouse">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/sunhousex80x40x4.webp" />
                            Bếp gas dương Sunhouse
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-duong.html" title="Xem tất cả Bếp gas dương">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-doi-mat-kinh-viet-nhat.html" title="Bếp Gas Đôi Mặt Kính Việt Nhật ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-doi-mat-kinh-viet-nhatx500x500x4.webp" alt="Bếp Gas Đôi Mặt Kính Việt Nhật " title="Bếp Gas Đôi Mặt Kính Việt Nhật ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-doi-mat-kinh-viet-nhat.html" title="Bếp Gas Đôi Mặt Kính Việt Nhật ">Bếp Gas Đôi Mặt Kính Việt Nhật </a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">380.000₫</p>

                                <p>
                                    <span>660.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-42%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-doi-paloma-pa-j51ce.html" title="Bếp gas đôi Paloma PA - J51CE">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/10047962-bep-gas-doi-paloma-pa-j51ce-1x500x500x4.webp" alt="Bếp gas đôi Paloma PA - J51CE" title="Bếp gas đôi Paloma PA - J51CE">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-doi-paloma-pa-j51ce.html" title="Bếp gas đôi Paloma PA - J51CE">Bếp gas đôi Paloma PA - J51CE</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Paloma" src="/Data/ResizeImage/files/banner/logo hang sx/palomax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.550.000₫</p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-canaval-ca-366.html" title="Bếp Gas Dương Canaval CA 366">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/adv/CA-366x500x500x4.webp" alt="Bếp Gas Dương Canaval CA 366" title="Bếp Gas Dương Canaval CA 366">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-canaval-ca-366.html" title="Bếp Gas Dương Canaval CA 366">Bếp Gas Dương Canaval CA 366</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="CANAVAL" src="/Data/ResizeImage/images/adv/cropped-Untitled-1-copyx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.880.000₫</p>

                                <p class="promotion">Tặng <b>MÁY HÚT MÙI CANAVAL CA 8700SS</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-rinnai-rv-8611-gl-b.html" title="Bếp gas dương Rinnai RV-8611(GL-B)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bep-bluger/bep-gas-duong-rinnai-rv-8611-gl-bx500x500x4.webp" alt="Bếp gas dương Rinnai RV-8611(GL-B)" title="Bếp gas dương Rinnai RV-8611(GL-B)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-rinnai-rv-8611-gl-b.html" title="Bếp gas dương Rinnai RV-8611(GL-B)">Bếp gas dương Rinnai RV-8611(GL-B)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.580.000₫</p>

                                <p>
                                    <span>1.880.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-15%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-rinnai-rv-260g.html" title="Bếp gas dương Rinnai RV-260G">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bep-bluger/rinnai-gv-260gx500x500x4.webp" alt="Bếp gas dương Rinnai RV-260G" title="Bếp gas dương Rinnai RV-260G">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-rinnai-rv-260g.html" title="Bếp gas dương Rinnai RV-260G">Bếp gas dương Rinnai RV-260G</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Rinnai" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">920.000₫</p>

                                <p>
                                    <span>1.050.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-12%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-taka-kg5.html" title="Bếp gas dương Taka KG5">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-duong-taka-kg5x500x500x4.webp" alt="Bếp gas dương Taka KG5" title="Bếp gas dương Taka KG5">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-taka-kg5.html" title="Bếp gas dương Taka KG5">Bếp gas dương Taka KG5</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Taka" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.160.000₫</p>

                                <p>
                                    <span>1.400.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-17%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-am-hong-ngoai.html" title="Bếp gas âm hồng ngoại">Bếp gas âm hồng ngoại</a>
                        </h2>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-am-hong-ngoai.html" title="Xem tất cả Bếp gas âm hồng ngoại">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-hong-ngoai-blueger-b217gc.html" title="Bếp gas âm hồng ngoại Blueger B217GC">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-hong-ngoai-blueger-b217gcx500x500x4.webp" alt="Bếp gas âm hồng ngoại Blueger B217GC" title="Bếp gas âm hồng ngoại Blueger B217GC">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-hong-ngoai-blueger-b217gc.html" title="Bếp gas âm hồng ngoại Blueger B217GC">Bếp gas âm hồng ngoại Blueger B217GC</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Blueger" src="/Data/ResizeImage/files/adv/logo-thuonghieu/logo-blueger-1024x354x150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.350.000₫</p>

                                <p>
                                    <span>3.250.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-27%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột  </b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-hong-ngoai-taka-bg02c.html" title="Bếp gas âm hồng ngoại Taka BG02C">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-hong-ngoai-taka-bg02cx500x500x4.webp" alt="Bếp gas âm hồng ngoại Taka BG02C" title="Bếp gas âm hồng ngoại Taka BG02C">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-hong-ngoai-taka-bg02c.html" title="Bếp gas âm hồng ngoại Taka BG02C">Bếp gas âm hồng ngoại Taka BG02C</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Taka" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.150.000₫</p>

                                <p>
                                    <span>2.950.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-27%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-hong-ngoai-taka-bg02c-do-den.html" title="Bếp gas âm hồng ngoại Taka BG02C (đỏ đen)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-hong-ngoai-taka-bg02c-do-denx500x500x4.webp" alt="Bếp gas âm hồng ngoại Taka BG02C (đỏ đen)" title="Bếp gas âm hồng ngoại Taka BG02C (đỏ đen)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-hong-ngoai-taka-bg02c-do-den.html" title="Bếp gas âm hồng ngoại Taka BG02C (đỏ đen)">Bếp gas âm hồng ngoại Taka BG02C (đỏ đen)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Taka" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.210.000₫</p>

                                <p>
                                    <span>3.200.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-30%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-hong-ngoai-redsun-rs-083d.html" title="Bếp gas âm hồng ngoại Redsun RS-083D">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-hong-ngoai-redsun-rs-083dx500x500x4.webp" alt="Bếp gas âm hồng ngoại Redsun RS-083D" title="Bếp gas âm hồng ngoại Redsun RS-083D">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-hong-ngoai-redsun-rs-083d.html" title="Bếp gas âm hồng ngoại Redsun RS-083D">Bếp gas âm hồng ngoại Redsun RS-083D</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Redsun" src="/Data/ResizeImage/files/banner/logo hang sx/redsunx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.435.000₫</p>

                                <p>
                                    <span>2.680.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-9%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-hong-ngoai-redsun-rs-002a.html" title="Bếp gas âm hồng ngoại Redsun RS-002A">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-hong-ngoai-redsun-rs-002ax500x500x4.webp" alt="Bếp gas âm hồng ngoại Redsun RS-002A" title="Bếp gas âm hồng ngoại Redsun RS-002A">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-hong-ngoai-redsun-rs-002a.html" title="Bếp gas âm hồng ngoại Redsun RS-002A">Bếp gas âm hồng ngoại Redsun RS-002A</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Redsun" src="/Data/ResizeImage/files/banner/logo hang sx/redsunx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">4.150.000₫</p>

                                <p>
                                    <span>5.350.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-22%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-hong-ngoai-redsun-rs-001a.html" title="Bếp gas âm hồng ngoại Redsun RS-001A">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-am-hong-ngoai-redsun-rs-001ax500x500x4.webp" alt="Bếp gas âm hồng ngoại Redsun RS-001A" title="Bếp gas âm hồng ngoại Redsun RS-001A">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-hong-ngoai-redsun-rs-001a.html" title="Bếp gas âm hồng ngoại Redsun RS-001A">Bếp gas âm hồng ngoại Redsun RS-001A</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Redsun" src="/Data/ResizeImage/files/banner/logo hang sx/redsunx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">3.910.000₫</p>

                                <p>
                                    <span>5.350.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-26%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-duong-hong-ngoai.html" title="Bếp gas dương hồng ngoại">Bếp gas dương hồng ngoại</a>
                        </h2>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-duong-hong-ngoai.html" title="Xem tất cả Bếp gas dương hồng ngoại">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-kinh-hong-ngoai-tk-120d.html" title="BẾP GAS DƯƠNG KÍNH HỒNG NGOẠI TK 120D">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/539_untitledx500x500x4.webp" alt="BẾP GAS DƯƠNG KÍNH HỒNG NGOẠI TK 120D" title="BẾP GAS DƯƠNG KÍNH HỒNG NGOẠI TK 120D">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-kinh-hong-ngoai-tk-120d.html" title="BẾP GAS DƯƠNG KÍNH HỒNG NGOẠI TK 120D">BẾP GAS DƯƠNG KÍNH HỒNG NGOẠI TK 120D</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Taka" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.250.000₫</p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-redsun-rs-928k.html" title="Bếp gas dương Redsun RS-928K">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-duong-redsun-rs-928kx500x500x4.webp" alt="Bếp gas dương Redsun RS-928K" title="Bếp gas dương Redsun RS-928K">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-redsun-rs-928k.html" title="Bếp gas dương Redsun RS-928K">Bếp gas dương Redsun RS-928K</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Redsun" src="/Data/ResizeImage/files/banner/logo hang sx/redsunx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.950.000₫</p>

                                <p>
                                    <span>3.690.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-47%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-redsun-rs-928h.html" title="Bếp gas dương Redsun RS-928H">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-duong-redsun-rs-928hx500x500x4.webp" alt="Bếp gas dương Redsun RS-928H" title="Bếp gas dương Redsun RS-928H">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-redsun-rs-928h.html" title="Bếp gas dương Redsun RS-928H">Bếp gas dương Redsun RS-928H</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Redsun" src="/Data/ResizeImage/files/banner/logo hang sx/redsunx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.950.000₫</p>

                                <p>
                                    <span>3.690.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-47%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-redsun-rs-828m.html" title="Bếp gas dương Redsun RS-828M">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-duong-redsun-rs-828mx500x500x4.webp" alt="Bếp gas dương Redsun RS-828M" title="Bếp gas dương Redsun RS-828M">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-redsun-rs-828m.html" title="Bếp gas dương Redsun RS-828M">Bếp gas dương Redsun RS-828M</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Redsun" src="/Data/ResizeImage/files/banner/logo hang sx/redsunx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.410.000₫</p>

                                <p>
                                    <span>2.690.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-10%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-hong-ngoai-blueger-b72ga.html" title="Bếp gas dương hồng ngoại Blueger B72GA">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-duong-hong-ngoai-blueger-b72gax500x500x4.webp" alt="Bếp gas dương hồng ngoại Blueger B72GA" title="Bếp gas dương hồng ngoại Blueger B72GA">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-hong-ngoai-blueger-b72ga.html" title="Bếp gas dương hồng ngoại Blueger B72GA">Bếp gas dương hồng ngoại Blueger B72GA</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Blueger" src="/Data/ResizeImage/files/adv/logo-thuonghieu/logo-blueger-1024x354x150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">950.000₫</p>

                                <p>
                                    <span>1.350.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-29%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột  </b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-duong-hong-ngoai-blueger-b72gb.html" title="Bếp gas dương hồng ngoại Blueger B72GB">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-duong-hong-ngoai-blueger-b72gbx500x500x4.webp" alt="Bếp gas dương hồng ngoại Blueger B72GB" title="Bếp gas dương hồng ngoại Blueger B72GB">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-duong-hong-ngoai-blueger-b72gb.html" title="Bếp gas dương hồng ngoại Blueger B72GB">Bếp gas dương hồng ngoại Blueger B72GB</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Blueger" src="/Data/ResizeImage/files/adv/logo-thuonghieu/logo-blueger-1024x354x150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">950.000₫</p>

                                <p>
                                    <span>1.300.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-26%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột  </b></p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-gia-re.html" title="Bếp gas giá rẻ">Bếp gas giá rẻ</a>
                        </h2>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-gia-re.html" title="Xem tất cả Bếp gas giá rẻ">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-doi-inox-viet-nhat-gia-re.html" title="Bếp gas đôi inox Việt Nhật giá rẻ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/bep-gas-doi-inox-fujishi-fu-210-in-chen-dong-2x500x500x4.webp" alt="Bếp gas đôi inox Việt Nhật giá rẻ" title="Bếp gas đôi inox Việt Nhật giá rẻ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-doi-inox-viet-nhat-gia-re.html" title="Bếp gas đôi inox Việt Nhật giá rẻ">Bếp gas đôi inox Việt Nhật giá rẻ</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">390.000₫</p>

                                <p>
                                    <span>490.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-20%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-doi-mat-kinh-viet-nhat.html" title="Bếp Gas Đôi Mặt Kính Việt Nhật  ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-doi-mat-kinh-viet-nhatx500x500x4.webp" alt="Bếp Gas Đôi Mặt Kính Việt Nhật  " title="Bếp Gas Đôi Mặt Kính Việt Nhật  ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-doi-mat-kinh-viet-nhat.html" title="Bếp Gas Đôi Mặt Kính Việt Nhật  ">Bếp Gas Đôi Mặt Kính Việt Nhật  </a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">380.000₫</p>

                                <p>
                                    <span>660.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-42%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-sunhouse-shb5538.html" title="Bếp gas âm Sunhouse SHB-5538">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/bep-gas-am-kinh-sunhouse-shb5538x500x500x4.webp" alt="Bếp gas âm Sunhouse SHB-5538" title="Bếp gas âm Sunhouse SHB-5538">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-sunhouse-shb5538.html" title="Bếp gas âm Sunhouse SHB-5538">Bếp gas âm Sunhouse SHB-5538</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Sunhouse" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/sunhousex150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.300.000₫</p>

                                <p>
                                    <span>3.050.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-24%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-doi-lien-doanh-gia-re.html" title="Bếp gas đôi liên doanh giá rẻ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/B-p-Gas--i-Bexlim-V2-web-b-p-compressedx500x500x4.webp" alt="Bếp gas đôi liên doanh giá rẻ" title="Bếp gas đôi liên doanh giá rẻ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-doi-lien-doanh-gia-re.html" title="Bếp gas đôi liên doanh giá rẻ">Bếp gas đôi liên doanh giá rẻ</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">380.000₫</p>

                                <p>
                                    <span>650.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-41%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-taka-tk920a.html" title="Bếp gas âm Taka TK-920A">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/253_bep_gas_taka_tk105bx500x500x4.webp" alt="Bếp gas âm Taka TK-920A" title="Bếp gas âm Taka TK-920A">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-taka-tk920a.html" title="Bếp gas âm Taka TK-920A">Bếp gas âm Taka TK-920A</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Taka" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.850.000₫</p>

                                <p>
                                    <span>2.850.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-35%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-ga-am-vanessa-vs626.html" title="Bếp ga âm Vanessa VS626">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/626x500x500x4.webp" alt="Bếp ga âm Vanessa VS626" title="Bếp ga âm Vanessa VS626">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-ga-am-vanessa-vs626.html" title="Bếp ga âm Vanessa VS626">Bếp ga âm Vanessa VS626</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="VANESSA" src="/Data/ResizeImage/images/logo-vanessax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">2.200.000₫</p>

                                <p>
                                    <span>6.900.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-68%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Dây dẫn gas Hàn Quốc lõi thếp ba lớp chống chuột</b></p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-cong-nghiep.html" title="Bếp gas công nghiệp| Bếp gas nhà hàng, khách sạn">Bếp gas công nghiệp| Bếp gas nhà hàng, khách sạn</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bep-gas-cong-nghiep-gado.html" title="Bếp gas công nghiệp Gado">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Gadox80x40x4.webp" />
                            Bếp gas công nghiệp Gado
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-cong-nghiep-kendo.html" title="Bếp gas công nghiệp Kendo">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Kendox80x40x4.webp" />
                            Bếp gas công nghiệp Kendo
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-cong-nghiep-rinnai.html" title="Bếp gas công nghiệp Rinnai">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix80x40x4.webp" />
                            Bếp gas công nghiệp Rinnai
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-cong-nghiep-wonderful.html" title="Bếp gas công nghiệp Wonderful">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Wonderfulx80x40x4.webp" />
                            Bếp gas công nghiệp Wonderful
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-cong-nghiep.html" title="Xem tất cả Bếp gas công nghiệp| Bếp gas nhà hàng, khách sạn">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-cong-nghiep-viet-nam.html" title="Bếp gas công nghiệp Việt Nam">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-cong-nghiep-viet-namx500x500x4.webp" alt="Bếp gas công nghiệp Việt Nam" title="Bếp gas công nghiệp Việt Nam">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-cong-nghiep-viet-nam.html" title="Bếp gas công nghiệp Việt Nam">Bếp gas công nghiệp Việt Nam</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">550.000₫</p>

                                <p>
                                    <span>710.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-22%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-cong-nghiep-can-trung-viet-nam.html" title="Bếp gas công nghiệp cán trung Việt Nam">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/d8902252610e213bb54eda3c573a41e8x500x500x4.webp" alt="Bếp gas công nghiệp cán trung Việt Nam" title="Bếp gas công nghiệp cán trung Việt Nam">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-cong-nghiep-can-trung-viet-nam.html" title="Bếp gas công nghiệp cán trung Việt Nam">Bếp gas công nghiệp cán trung Việt Nam</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">560.000₫</p>

                                <p>
                                    <span>710.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-21%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-cong-nghiepcan-trung-gia-re.html" title="Bếp gas công nghiệp(Cán trung) giá rẻ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/large-02-2012-102-bep-gas-cong-nghiep-can-dai-1-500x500x500x500x4.webp" alt="Bếp gas công nghiệp(Cán trung) giá rẻ" title="Bếp gas công nghiệp(Cán trung) giá rẻ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-cong-nghiepcan-trung-gia-re.html" title="Bếp gas công nghiệp(Cán trung) giá rẻ">Bếp gas công nghiệp(Cán trung) giá rẻ</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">490.000₫</p>

                                <p>
                                    <span>690.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-28%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-cong-nghiep-gia-re.html" title="Bếp gas công nghiệp giá rẻ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-cong-nghiep-gia-rex500x500x4.webp" alt="Bếp gas công nghiệp giá rẻ" title="Bếp gas công nghiệp giá rẻ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-cong-nghiep-gia-re.html" title="Bếp gas công nghiệp giá rẻ">Bếp gas công nghiệp giá rẻ</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">410.000₫</p>

                                <p>
                                    <span>710.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-42%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-cong-nghiep-gado-gd-5a1e-can-ngan.html" title="Bếp gas công nghiệp Gado GD- 5A1E (cán ngắn)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bep-gas-cong-nghiep-gado-gd-5a2e-can-ngan_2803x500x500x4.webp" alt="Bếp gas công nghiệp Gado GD- 5A1E (cán ngắn)" title="Bếp gas công nghiệp Gado GD- 5A1E (cán ngắn)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-cong-nghiep-gado-gd-5a1e-can-ngan.html" title="Bếp gas công nghiệp Gado GD- 5A1E (cán ngắn)">Bếp gas công nghiệp Gado GD- 5A1E (cán ngắn)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Gado" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Gadox150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">690.000₫</p>

                                <p>
                                    <span>890.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-22%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-cong-nghiep-can-trung.html" title="Bếp gas công nghiệp(Cán trung)">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-cong-nghiep-can-trungx500x500x4.webp" alt="Bếp gas công nghiệp(Cán trung)" title="Bếp gas công nghiệp(Cán trung)">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-cong-nghiep-can-trung.html" title="Bếp gas công nghiệp(Cán trung)">Bếp gas công nghiệp(Cán trung)</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">750.000₫</p>

                                <p>
                                    <span>950.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-21%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-ket-hop-dien-tu.html" title="Bếp gas kết hợp điện từ">Bếp gas kết hợp điện từ</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bep-gas-ket-hop-dien-tu-faster.html" title="Bếp gas kết hợp điện từ Faster">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/fasterx80x40x4.webp" />
                            Bếp gas kết hợp điện từ Faster
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-ket-hop-dien-tu-brandt.html" title="Bếp gas kết hợp điện từ Brandt">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Brandtx80x40x4.webp" />
                            Bếp gas kết hợp điện từ Brandt
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-ket-hop-dien-tu-faber.html" title="Bếp gas kết hợp điện từ Faber">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/faberx80x40x4.webp" />
                            Bếp gas kết hợp điện từ Faber
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-ket-hop-dien-tu-dudoff.html" title="Bếp gas kết hợp điện từ Dudoff">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Dudoffx80x40x4.webp" />
                            Bếp gas kết hợp điện từ Dudoff
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-ket-hop-dien-tu-malloca.html" title="Bếp gas kết hợp điện từ Malloca">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/mallocax80x40x4.webp" />
                            Bếp gas kết hợp điện từ Malloca
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-ket-hop-dien-tu.html" title="Xem tất cả Bếp gas kết hợp điện từ">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-ket-hop-dien-faster-fs-921sttg.html" title="Bếp gas kết hợp điện Faster FS 921STTG">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-ket-hop-dien-faster-fs-921sttgx500x500x4.webp" alt="Bếp gas kết hợp điện Faster FS 921STTG" title="Bếp gas kết hợp điện Faster FS 921STTG">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-ket-hop-dien-faster-fs-921sttg.html" title="Bếp gas kết hợp điện Faster FS 921STTG">Bếp gas kết hợp điện Faster FS 921STTG</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Faster" src="/Data/ResizeImage/files/banner/logo hang sx/fasterx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">11.880.000₫</p>

                                <p>
                                    <span>14.900.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-20%)</span>

                                </p>

                                <p class="promotion">Tặng <b>Bộ nồi từ Fivestar 5 chiếc  </b></p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-ket-hop-tu-latino-lt02gt.html" title="Bếp gas kết hợp từ Latino LT-02GT">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/66_gas_tu_lt_02gtx500x500x4.webp" alt="Bếp gas kết hợp từ Latino LT-02GT" title="Bếp gas kết hợp từ Latino LT-02GT">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-ket-hop-tu-latino-lt02gt.html" title="Bếp gas kết hợp từ Latino LT-02GT">Bếp gas kết hợp từ Latino LT-02GT</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Latino" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Latinox150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">10.800.000₫</p>

                                <p>
                                    <span>13.000.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-16%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-malloca-eg-201-t.html" title="Bếp gas âm Malloca EG 201-T">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-malloca-eg-201-tx500x500x4.webp" alt="Bếp gas âm Malloca EG 201-T" title="Bếp gas âm Malloca EG 201-T">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-malloca-eg-201-t.html" title="Bếp gas âm Malloca EG 201-T">Bếp gas âm Malloca EG 201-T</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Malloca" src="/Data/ResizeImage/files/banner/logo hang sx/mallocax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">8.400.000₫</p>

                                <p>
                                    <span>9.330.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-9%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-malloca-eg-201-c.html" title="Bếp gas âm Malloca EG 201-C">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-malloca-eg-201-cx500x500x4.webp" alt="Bếp gas âm Malloca EG 201-C" title="Bếp gas âm Malloca EG 201-C">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-malloca-eg-201-c.html" title="Bếp gas âm Malloca EG 201-C">Bếp gas âm Malloca EG 201-C</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Malloca" src="/Data/ResizeImage/files/banner/logo hang sx/mallocax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">12.330.000₫</p>

                                <p>
                                    <span>13.700.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-10%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-malloca-eg-201.html" title="Bếp gas âm malloca EG 201">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-malloca-eg-201x500x500x4.webp" alt="Bếp gas âm malloca EG 201" title="Bếp gas âm malloca EG 201">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-malloca-eg-201.html" title="Bếp gas âm malloca EG 201">Bếp gas âm malloca EG 201</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Malloca" src="/Data/ResizeImage/files/banner/logo hang sx/mallocax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">9.780.000₫</p>

                                <p>
                                    <span>10.865.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-9%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-am-ket-hop-dien-malloca-eg-201h.html" title="Bếp gas âm kết hợp điện Malloca EG 201H">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-am-ket-hop-dien-malloca-eg-201hx500x500x4.webp" alt="Bếp gas âm kết hợp điện Malloca EG 201H" title="Bếp gas âm kết hợp điện Malloca EG 201H">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-am-ket-hop-dien-malloca-eg-201h.html" title="Bếp gas âm kết hợp điện Malloca EG 201H">Bếp gas âm kết hợp điện Malloca EG 201H</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Malloca" src="/Data/ResizeImage/files/banner/logo hang sx/mallocax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">9.780.000₫</p>

                                <p>
                                    <span>10.865.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-9%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bo-binh-bep-gas.html" title="Bếp và bình gas">Bếp và bình gas</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bo-binh-gas-petrolimex.html" title="Bộ bình gas Petrolimex">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/petrolimexx80x40x4.webp" />
                            Bộ bình gas Petrolimex
                        </a>

                        <a class="select" href="https://beptot.vn/bo-bep-gas-dynamic.html" title="Bộ bếp gas Dynamic">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/dynamic-2-x80x40x4.webp" />
                            Bộ bếp gas Dynamic
                        </a>

                        <a class="select" href="https://beptot.vn/bo-bep-gas-paloma.html" title="Bộ bếp gas Paloma">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/palomax80x40x4.webp" />
                            Bộ bếp gas Paloma
                        </a>

                        <a class="select" href="https://beptot.vn/bo-bep-gas-rinnai.html" title="Bộ bếp gas Rinnai">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix80x40x4.webp" />
                            Bộ bếp gas Rinnai
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bo-binh-bep-gas.html" title="Xem tất cả Bếp và bình gas">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-doi-hong-ngoai-mat-kinh-viet-nhat.html" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Việt Nhật">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/cad6790af1ea16b44ffbx500x500x4.webp" alt="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Việt Nhật" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Việt Nhật">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-doi-hong-ngoai-mat-kinh-viet-nhat.html" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Việt Nhật">Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Việt Nhật</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.290.000₫</p>

                                <p>
                                    <span>1.490.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-13%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-doi-mat-kinh-viet-nhat.html" title="Bộ Bếp Gas Đôi Mặt Kính Việt Nhật">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/bo-bep-gas-doi-mat-kinh-viet-nhat2x500x500x4.webp" alt="Bộ Bếp Gas Đôi Mặt Kính Việt Nhật" title="Bộ Bếp Gas Đôi Mặt Kính Việt Nhật">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-doi-mat-kinh-viet-nhat.html" title="Bộ Bếp Gas Đôi Mặt Kính Việt Nhật">Bộ Bếp Gas Đôi Mặt Kính Việt Nhật</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.250.000₫</p>

                                <p>
                                    <span>1.450.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-13%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-doi-mat-kinh-hong-ngoai.html" title="Bộ Bếp Gas Đôi Mặt Kính Hồng Ngoại">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/9b60dabe525eb500ec4fx500x500x4.webp" alt="Bộ Bếp Gas Đôi Mặt Kính Hồng Ngoại" title="Bộ Bếp Gas Đôi Mặt Kính Hồng Ngoại">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-doi-mat-kinh-hong-ngoai.html" title="Bộ Bếp Gas Đôi Mặt Kính Hồng Ngoại">Bộ Bếp Gas Đôi Mặt Kính Hồng Ngoại</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.110.000₫</p>

                                <p>
                                    <span>1.310.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-15%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-doi-hong-ngoai-mat-kinh.html" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/bo-bep-gas-doi-hong-ngoai-mat-kinh2x500x500x4.webp" alt="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính " title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-doi-hong-ngoai-mat-kinh.html" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính ">Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính </a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.250.000₫</p>

                                <p>
                                    <span>1.450.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-13%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-doi-hong-ngoai-mat-kinh-gia-re.html" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Giá Rẻ">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/Untitled-1x500x500x4.webp" alt="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Giá Rẻ" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Giá Rẻ">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-doi-hong-ngoai-mat-kinh-gia-re.html" title="Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Giá Rẻ">Bộ bếp Gas Đôi Hồng Ngoại Mặt Kính Giá Rẻ</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.010.000₫</p>

                                <p>
                                    <span>1.210.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-16%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-doi-mat-kinh-khuyen-mai.html" title="Bộ Bếp Gas Đôi Mặt Kính Khuyến mại">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/afdd03068be66cb835f7x500x500x4.webp" alt="Bộ Bếp Gas Đôi Mặt Kính Khuyến mại" title="Bộ Bếp Gas Đôi Mặt Kính Khuyến mại">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-doi-mat-kinh-khuyen-mai.html" title="Bộ Bếp Gas Đôi Mặt Kính Khuyến mại">Bộ Bếp Gas Đôi Mặt Kính Khuyến mại</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.250.000₫</p>

                                <p>
                                    <span>1.450.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-13%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-don.html" title="Bếp gas đơn">Bếp gas đơn</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bep-gas-don-namilux.html" title="Bếp gas đơn Namilux">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx80x40x4.webp" />
                            Bếp gas đơn Namilux
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-don-dynamic.html" title="Bếp gas đơn Dynamic">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/dynamic-2-x80x40x4.webp" />
                            Bếp gas đơn Dynamic
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-don-paloma.html" title="Bếp gas đơn Paloma">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/palomax80x40x4.webp" />
                            Bếp gas đơn Paloma
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-don-rinnai.html" title="Bếp gas đơn Rinnai">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Rinnaix80x40x4.webp" />
                            Bếp gas đơn Rinnai
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-don-sunhouse.html" title="Bếp gas đơn Sunhouse">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/sunhousex80x40x4.webp" />
                            Bếp gas đơn Sunhouse
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-don.html" title="Xem tất cả Bếp gas đơn">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-don-hong-ngoai-kio-350sg.html" title="BẾP GAS ĐƠN HỒNG NGOẠI KIO 350SG">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/newproduct/2fdd645ed2b9c022711ec778c249bc26x500x500x4.webp" alt="BẾP GAS ĐƠN HỒNG NGOẠI KIO 350SG" title="BẾP GAS ĐƠN HỒNG NGOẠI KIO 350SG">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-don-hong-ngoai-kio-350sg.html" title="BẾP GAS ĐƠN HỒNG NGOẠI KIO 350SG">BẾP GAS ĐƠN HỒNG NGOẠI KIO 350SG</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">450.000₫</p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-don-namilux-na-300asm.html" title="Bếp gas đơn Namilux NA-300ASM">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bep-gas-don-namilux-na-300asmx500x500x4.webp" alt="Bếp gas đơn Namilux NA-300ASM" title="Bếp gas đơn Namilux NA-300ASM">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-don-namilux-na-300asm.html" title="Bếp gas đơn Namilux NA-300ASM">Bếp gas đơn Namilux NA-300ASM</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Namilux" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">350.000₫</p>

                                <p>
                                    <span>650.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-46%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-don-hong-ngoai-shapo-japan.html" title="Bếp gas đơn hồng ngoại  Shapo Japan">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-don-hong-ngoai-shapo-japanx500x500x4.webp" alt="Bếp gas đơn hồng ngoại  Shapo Japan" title="Bếp gas đơn hồng ngoại  Shapo Japan">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-don-hong-ngoai-shapo-japan.html" title="Bếp gas đơn hồng ngoại  Shapo Japan">Bếp gas đơn hồng ngoại  Shapo Japan</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">450.000₫</p>

                                <p>
                                    <span>650.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-30%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bo-bep-gas-don-hong-ngoai.html" title="Bộ bếp gas đơn hồng ngoại">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bo-bep-gas-don-hong-ngoaix500x500x4.webp" alt="Bộ bếp gas đơn hồng ngoại" title="Bộ bếp gas đơn hồng ngoại">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bo-bep-gas-don-hong-ngoai.html" title="Bộ bếp gas đơn hồng ngoại">Bộ bếp gas đơn hồng ngoại</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">910.000₫</p>

                                <p>
                                    <span>1.250.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-27%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-don-kinh-hong-ngoai-1.html" title="Bếp gas đơn kính hồng ngoại">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-don-kinh-hong-ngoai-1x500x500x4.webp" alt="Bếp gas đơn kính hồng ngoại" title="Bếp gas đơn kính hồng ngoại">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-don-kinh-hong-ngoai-1.html" title="Bếp gas đơn kính hồng ngoại">Bếp gas đơn kính hồng ngoại</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">350.000₫</p>

                                <p>
                                    <span>450.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-22%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-hong-ngoai-taka-tk-01a.html" title="BẾP GAS HỒNG NGOẠI TAKA TK-01A">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-hong-ngoai-taka-tk-01ax500x500x4.webp" alt="BẾP GAS HỒNG NGOẠI TAKA TK-01A" title="BẾP GAS HỒNG NGOẠI TAKA TK-01A">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-hong-ngoai-taka-tk-01a.html" title="BẾP GAS HỒNG NGOẠI TAKA TK-01A">BẾP GAS HỒNG NGOẠI TAKA TK-01A</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Taka" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/takax150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">550.000₫</p>

                                <p>
                                    <span>890.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-38%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/bep-gas-du-lich.html" title="Bếp gas du lịch">Bếp gas du lịch</a>
                        </h2>

                        <a class="select" href="https://beptot.vn/bep-gas-du-lich-redsun.html" title="Bếp gas du lịch Redsun">

                            <img src="/Data/ResizeImage/files/banner/logo hang sx/redsunx80x40x4.webp" />
                            Bếp gas du lịch Redsun
                        </a>

                        <a class="select" href="https://beptot.vn/bep-gas-du-lich-namilux.html" title="Bếp gas du lịch Namilux">

                            <img src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx80x40x4.webp" />
                            Bếp gas du lịch Namilux
                        </a>

                        <a class="select view-all" href="https://beptot.vn/bep-gas-du-lich.html" title="Xem tất cả Bếp gas du lịch">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-du-lich-namilux-1911.html" title="BẾP GAS DU LỊCH NAMILUX 1911">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/0d68662231044d5d08dd688ee7cea6d6x500x500x4.webp" alt="BẾP GAS DU LỊCH NAMILUX 1911" title="BẾP GAS DU LỊCH NAMILUX 1911">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-du-lich-namilux-1911.html" title="BẾP GAS DU LỊCH NAMILUX 1911">BẾP GAS DU LỊCH NAMILUX 1911</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Namilux" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">270.000₫</p>

                                <p>
                                    <span>490.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-44%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-du-lich-namilux.html" title="Bếp gas du lịch Namilux">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-du-lich-namiluxx500x500x4.webp" alt="Bếp gas du lịch Namilux" title="Bếp gas du lịch Namilux">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-du-lich-namilux.html" title="Bếp gas du lịch Namilux">Bếp gas du lịch Namilux</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Namilux" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">260.000₫</p>

                                <p>
                                    <span>355.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-26%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-bep-gas-du-lich-namilux-na-243pn.html" title="Bếp gas du lịch Namilux NA-243PN">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-bep-gas-du-lich-namilux-na-243pnx500x500x4.webp" alt="Bếp gas du lịch Namilux NA-243PN" title="Bếp gas du lịch Namilux NA-243PN">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-bep-gas-du-lich-namilux-na-243pn.html" title="Bếp gas du lịch Namilux NA-243PN">Bếp gas du lịch Namilux NA-243PN</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Namilux" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">250.000₫</p>

                                <p>
                                    <span>360.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-30%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-du-lich-namilux-na-164.html" title="Bếp gas du lịch Namilux NA-164">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-du-lich-namilux-na-164x500x500x4.webp" alt="Bếp gas du lịch Namilux NA-164" title="Bếp gas du lịch Namilux NA-164">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-du-lich-namilux-na-164.html" title="Bếp gas du lịch Namilux NA-164">Bếp gas du lịch Namilux NA-164</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Namilux" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">420.000₫</p>

                                <p>
                                    <span>490.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-14%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-du-lich-hong-ngoai-namilux-na-171as.html" title="Bếp gas du lịch hồng ngoại Namilux NA-171AS">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/bep-gas-du-lich-hong-ngoai-namilux-na-171asx500x500x4.webp" alt="Bếp gas du lịch hồng ngoại Namilux NA-171AS" title="Bếp gas du lịch hồng ngoại Namilux NA-171AS">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-du-lich-hong-ngoai-namilux-na-171as.html" title="Bếp gas du lịch hồng ngoại Namilux NA-171AS">Bếp gas du lịch hồng ngoại Namilux NA-171AS</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Namilux" src="/Data/ResizeImage/files/adv/1-logo-thuong-hieu-0306/Namiluxx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">420.000₫</p>

                                <p>
                                    <span>490.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-14%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/bep-gas-du-lich-son.html" title="Bếp gas du lịch sơn">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/large_bep-gas-du-lich-sonx500x500x4.webp" alt="Bếp gas du lịch sơn" title="Bếp gas du lịch sơn">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/bep-gas-du-lich-son.html" title="Bếp gas du lịch sơn">Bếp gas du lịch sơn</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">160.000₫</p>

                                <p>
                                    <span>225.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-28%)</span>

                                </p>

                            </div>
                        </li>

                    </ul>
                </section>

                <section class="box-category">
                    <div class="sub_header">
                        <h2 class="title">
                            <a href="https://beptot.vn/binh-gas-linh-kien.html" title="Bình gas & Linh kiện">Bình gas & Linh kiện</a>
                        </h2>

                        <a class="select view-all" href="https://beptot.vn/binh-gas-linh-kien.html" title="Xem tất cả Bình gas & Linh kiện">Xem tất cả</a>
                    </div>

                    <ul class="list_product_featured ">

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/binh-nong-lanh-gas-owani-d322.html" title="BÌNH NÓNG LẠNH GAS OWANI - D322">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/files/unnamed-1-x500x500x4.webp" alt="BÌNH NÓNG LẠNH GAS OWANI - D322" title="BÌNH NÓNG LẠNH GAS OWANI - D322">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/binh-nong-lanh-gas-owani-d322.html" title="BÌNH NÓNG LẠNH GAS OWANI - D322">BÌNH NÓNG LẠNH GAS OWANI - D322</a></h3>
                            </div>
                            <div class="cate_pro_title">

                                <a href="javascript:void(0)">
                                    <img alt="Owani" src="/Data/ResizeImage/files/logo-hang/OWANIx150x40x4.webp">
                                </a>

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">1.350.000₫</p>

                                <p>
                                    <span>1.750.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-22%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/kho-gas-cong-nghiep.html" title="Khò gas công nghiệp">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/bếp gas/715335-500x500x500x500x4.webp" alt="Khò gas công nghiệp" title="Khò gas công nghiệp">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/kho-gas-cong-nghiep.html" title="Khò gas công nghiệp">Khò gas công nghiệp</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">260.000₫</p>

                                <p>
                                    <span>360.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-27%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/van-cau-tri-tu-ngat-viet-nhat.html" title="Van cầu trì tự ngắt việt nhật">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/van-cau-tri-tu-ngat-viet-nhatx500x500x4.webp" alt="Van cầu trì tự ngắt việt nhật" title="Van cầu trì tự ngắt việt nhật">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/van-cau-tri-tu-ngat-viet-nhat.html" title="Van cầu trì tự ngắt việt nhật">Van cầu trì tự ngắt việt nhật</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">220.000₫</p>

                                <p>
                                    <span>325.000₫</span>

                                    <span class="cate_pro_bot-saleof">(-32%)</span>

                                </p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/binh-gas-petrolimex-12kg-van-ngang.html" title="Bình gas Petrolimex 12kg van ngang">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/binh-gas-petrolimex-12kg-van-ngangx500x500x4.webp" alt="Bình gas Petrolimex 12kg van ngang" title="Bình gas Petrolimex 12kg van ngang">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/binh-gas-petrolimex-12kg-van-ngang.html" title="Bình gas Petrolimex 12kg van ngang">Bình gas Petrolimex 12kg van ngang</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">Liên hệ: 0986.083.083</p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/binh-gas-shell-gas-12kg.html" title="Bình gas shell gas 12kg">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/binh-gas-shell-gas-12kgx500x500x4.webp" alt="Bình gas shell gas 12kg" title="Bình gas shell gas 12kg">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/binh-gas-shell-gas-12kg.html" title="Bình gas shell gas 12kg">Bình gas shell gas 12kg</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">Liên hệ: 0986.083.083</p>

                            </div>
                        </li>

                        <li>
                            <div class="cate_pro_top">
                                <figure>
                                    <a class="img" href="https://beptot.vn/binh-gas-do-phap.html" title="Bình gas đỏ pháp">
                                        <img class="lazy owl-lazy" data-src="/Data/ResizeImage/images/product/beptot/binh-gas-do-phapx500x500x4.webp" alt="Bình gas đỏ pháp" title="Bình gas đỏ pháp">
                                    </a>
                                </figure>
                                <h3><a href="https://beptot.vn/binh-gas-do-phap.html" title="Bình gas đỏ pháp">Bình gas đỏ pháp</a></h3>
                            </div>
                            <div class="cate_pro_title">

                            </div>
                            <div class="cate_pro_bot">

                                <p class="price-now">Liên hệ: 0986.083.083</p>

                            </div>
                        </li>

                    </ul>
                </section>

            </div>
        </div>
    </div>
    <footer id="footer">
        <section class="footer_top">
            <div class="container">
                <div class="row">
                    <ul class="list-footer">


                        <li>
                            <div class="footer-title">
                                <div class="logo-f">
                                    <a href="index.html">
                                        <img src="/Data/upload/files/logobeptot2.png" alt=""></a>
                                </div>
                            </div>
                            <div class="bg-ft">
                                <ul class="list-mn">
                                    <li>
                                        <span>Mua hàng <a href="tel:024 33 100 100"><strong>024 33 100 100</strong></a> (7:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <span>Bảo hành <a href="tel:024 35 122 122"><strong>024 35 122 122 </strong></a>(8:00 - 19:00)</span>
                                    </li>
                                    <li>
                                        <span>Khiếu nại <a href="tel:0912 220 883"><strong>0912 220 883</strong></a> (8:00 - 19:00)</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="mb10">
                                <h3>Bạn có thể thanh toán với</h3>
                                <div class="payment-img">
                                    <img class="icon" src="/Content/desktop/images/visa.svg" width="54" alt="">
                                    <img class="icon" src="/Content/desktop/images/mastercard.svg" width="54" alt="">
                                    <img class="icon" src="/Content/desktop/images/jcb.svg" width="54" alt="">
                                    <img class="icon" src="/Content/desktop/images/cash.svg" width="54" alt="">
                                    <img class="icon" src="/Content/desktop/images/internet-banking.svg" width="54" alt="">
                                    <img class="icon" src="/Content/desktop/images/installment.svg" width="54" alt="">
                                </div>
                            </div>
                        </li>


                        <li>
                            <p class="heading">Thông tin công ty</p>
                            <div class="bg-ft">
                                <ul class="list-mn">

                                    <li><a rel="nofollow" href="https://beptot.vn/gioi-thieu.html" title="Giới thiệu">Giới thiệu</a></li>

                                    <li><a rel="nofollow" href="https://beptot.vn/tuyen-dung.html" title="Tuyển dụng">Tuyển dụng</a></li>

                                </ul>
                            </div>
                        </li>


                        <li>
                            <p class="heading">Hỗ trợ khách hàng</p>
                            <div class="bg-ft">
                                <ul class="list-mn">

                                    <li><a rel="nofollow" href="https://beptot.vn/chinh-sach-thanh-toan.html" title="Chính Sách Thanh Toán">Chính Sách Thanh Toán</a></li>

                                    <li><a rel="nofollow" href="https://beptot.vn/chinh-sach-van-chuyen.html" title="Chính sách vận chuyển">Chính sách vận chuyển</a></li>

                                    <li><a rel="nofollow" href="https://beptot.vn/chinh-sach-doi-tra.html" title="Chính Sách Đổi Trả">Chính Sách Đổi Trả</a></li>

                                    <li><a rel="nofollow" href="https://beptot.vn/chinh-sach-bao-hanh.html" title="Chính Sách Bảo Hành">Chính Sách Bảo Hành</a></li>

                                </ul>
                            </div>
                        </li>


                        <li>

                            <div class="single-text res-text">
                                <div class="mb10">
                                    <h3>Kết nối với chúng tôi</h3>
                                    <div class="social-footer">
                                        <a href="" title="Facebook" rel="nofollow"><i class="fa fa-facebook-square"></i></a>
                                        <a href="" title="Twitter" rel="nofollow"><i class="fa fa-twitter"></i></a>
                                        <a href="" title="Instagram" rel="nofollow"><i class="fa fa-instagram"></i></a>
                                        <a href="" title="Youtube" rel="nofollow"><i class="fa fa-youtube-play"></i></a>
                                    </div>
                                </div>
                                <a href="https://www.facebook.com/beptot.vn">
                                    <img class="lazy" src="/Content/desktop/images/fb_beptot.png" />
                                </a>

                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </section>
        <section class="footer_middle">
            <div class="container">
                <div class="row">
                    <div>
                        <div class="row">
                            <div class="col-md-12 title-showroom">HỆ THỐNG SHOWROOM BEPTOT.VN</div>
                            <div class="col-md-4 footer-new-left">

                                <h3>Showroom Bếp Tốt khu vực Hà Nội</h3>

                                <button type="button" class="address-link" data-value="1" id="map9" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.6689115350373!2d105.78263745076237!3d21.045929692498397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab32f8d4d46f%3A0x34a64e2c694cbcd2!2zNDY3IEhvw6BuZyBRdeG7kWMgVmnhu4d0LCBD4buVIE5odeG6vywgQuG6r2MgVOG7qyBMacOqbSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1636707268323!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('9','BEPTOT.VN - 467 HOÀNG QUỐC VIỆT - CẦU GIẤY - HÀ NỘI','0986 083 083','024 35 122 122','467 HOÀNG QUỐC VIỆT - CẦU GIẤY - HÀ NỘI (Có Chỗ Để Xe Ô Tô)');"><i class="fa fa-home icon-2"></i>467 HOÀNG QUỐC VIỆT - CẦU GIẤY - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map3" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6138410108474!2d105.81282831482022!3d21.008110993859024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ad01719efb19%3A0xbfa8c5f57553d165!2sBeptot.vn!5e0!3m2!1svi!2s!4v1607660132104!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen=''></iframe>" onclick="ShowMap('3','BEPTOT.VN - 326 - 330 ĐƯỜNG LÁNG - ĐỐNG ĐA - HÀ NỘI','  0986 083 083',' 024 33 100 100','326 - 330 ĐƯỜNG LÁNG - ĐỐNG ĐA - HÀ NỘI ( Chân Cầu Đường Sắt Trên Cao. Có Chỗ Để Xe Ô Tô ) ');"><i class="fa fa-home icon-2"></i>326 - 330 ĐƯỜNG LÁNG - ĐỐNG ĐA - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map10" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2633.0251447917185!2d105.87622639281805!3d21.046291253088874!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a97e10e11aa7%3A0xc8864fce0c75ab0d!2zMzUzIMSQLiBOZ3V54buFbiBWxINuIEPhu6ssIE5n4buNYyBMw6JtLCBMb25nIEJpw6puLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1634786688507!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('10','BEPTOT.VN - 353 NGUYỄN VĂN CỪ - LONG BIÊN - HÀ NỘI','0986 113 360 - 0987 764 646','0364 322 322','353 NGUYỄN VĂN CỪ - LONG BIÊN - HÀ NỘI ( Có Chỗ Để Xe Ô Tô )');"><i class="fa fa-home icon-2"></i>353 NGUYỄN VĂN CỪ - LONG BIÊN - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map1" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.682162702125!2d105.87337441424566!3d21.04539979258755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abd606c37577%3A0x3a7f1087ac18e687!2zMzM4IE5ndXnhu4VuIFbEg24gQ-G7qywgTmfhu41jIEzDom0sIExvbmcgQmnDqm4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1567655869505!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen=''></iframe>" onclick="ShowMap('1','BEPTOT.VN - 338 NGUYỄN VĂN CỪ - LONG BIÊN - HÀ NỘI','0986 113 360 - 0987 764 646','024 35 122 122','338 NGUYỄN VĂN CỪ - LONG BIÊN - HÀ NỘI ( Đối Diện Siêu Thị Nguyễn Kim, Có Chỗ Để Xe Ô Tô )');"><i class="fa fa-home icon-2"></i>338 NGUYỄN VĂN CỪ - LONG BIÊN - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map22" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6264.93163057942!2d105.79235293484061!3d20.986598028605933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acc63d098f2f%3A0x4349ab624fb7e54d!2zNjA5IE5ndXnhu4VuIFRyw6NpLCBQLiBWxINuIFF1w6FuLCBIw6AgxJDDtG5nLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1638854649618!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('22','BEPTOT.VN - 609 NGUYỄN TRÃI - VĂN QUÁN - HÀ NỘI','  0986 083 083',' 024 33 100 100','609 NGUYỄN TRÃI - VĂN QUÁN - HÀ NỘI ( Đối Diện Siêu Thị Pico, Có Chỗ Để Xe Ô Tô )');"><i class="fa fa-home icon-2"></i>609 NGUYỄN TRÃI - VĂN QUÁN - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map13" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6883047315987!2d105.84905831530774!3d21.00512798601118!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac752321bc6d%3A0xb4dc0b69f6cec84e!2zMTc1IELhuqFjaCBNYWksIFRoYW5oIE5ow6BuLCBIYWkgQsOgIFRyxrBuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1592125293345!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('13','BEPTOT.VN - 175 BẠCH MAI - HAI BÀ TRƯNG - HÀ NỘI','0364 322 322','024 35 122 122','175 BẠCH MAI - HAI BÀ TRƯNG - HÀ NỘI');"><i class="fa fa-home icon-2"></i>175 BẠCH MAI - HAI BÀ TRƯNG - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map23" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2214.399352682273!2d105.79820966518682!3d21.026244843011387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab448e8eb5cb%3A0x57781c73d62e63b5!2zMTEzOCDEkC4gTMOhbmcsIFnDqm4gSG_DoCwgxJDhu5FuZyDEkGEsIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1615534961895!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('23','BEPTOT.VN - 1138 ĐƯỜNG LÁNG - ĐỐNG ĐA - HÀ NỘI','0962 686 787','024 35 122 122','1138 ĐƯỜNG LÁNG - ĐỐNG ĐA - HÀ NỘI');"><i class="fa fa-home icon-2"></i>1138 ĐƯỜNG LÁNG - ĐỐNG ĐA - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map2" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722.9738157538545!2d105.79819021424606!3d21.073707991619695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aac385361e6d%3A0x68e0b24c4c984217!2zMzU5IMSQxrDhu51uZyBOZ3V54buFbiBIb8OgbmcgVMO0biwgWHXDom4gxJDhu4luaCwgVOG7qyBMacOqbSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1566978527836!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen=''></iframe>" onclick="ShowMap('2','BEPTOT.VN - 359 NGUYỄN HOÀNG TÔN - TÂY HỒ - HÀ NỘI','0964 221 297','024 37 500 500','359 NGUYỄN HOÀNG TÔN - TÂY HỒ - HÀ NỘI (Có Chỗ Để Xe Ô Tô) ');"><i class="fa fa-home icon-2"></i>359 NGUYỄN HOÀNG TÔN - TÂY HỒ - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map6" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722.648699967826!2d105.91621731424634!3d21.086688691175567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a9d3eb297b2f%3A0xf2be0bd39f4d5ad4!2zMzQ1IEjDoCBIdXkgVOG6rXAsIFnDqm4gVmnDqm4sIEdpYSBMw6JtLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1574825323438!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen=''></iframe>" onclick="ShowMap('6','BEPTOT.VN - 345 HÀ HUY TẬP - GIA LÂM - HÀ NỘI','0986 113 360','0912 220 883',' 345 HÀ HUY TẬP - GIA LÂM - HÀ NỘI');"><i class="fa fa-home icon-2"></i>345 HÀ HUY TẬP - GIA LÂM - HÀ NỘI<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map20" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29776.887819731015!2d105.63187102293581!3d21.108141287340448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345630259b3aa1%3A0x6d97a1b83b06be3d!2zUGjGsMahbmcgxJDDrG5oLCDEkGFuIFBoxrDhu6NuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1611631308589!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('20','XƯỞNG SẢN XUẤT TỦ BẾP - PHƯƠNG ĐÌNH - ĐAN PHƯỢNG','0985 329 668','092 326 8888','Xã Phương Đình - Huyện Đan Phượng - Hà Nội');"><i class="fa fa-home icon-2"></i>XƯỞNG SẢN XUẤT TỦ BẾP - PHƯƠNG ĐÌNH - ĐAN PHƯỢNG<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <button type="button" class="address-link" data-value="1" id="map4" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14895.83804480077!2d105.738556!3d21.034306!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345489743d6921%3A0x7d10b435a7f2b06!2zMjQ5IMSQxrDhu51uZyBYdcOibiBQaMawxqFuZywgVsOibiBDYW5oLCBU4burIExpw6ptLCBIw6AgTuG7mWk!5e0!3m2!1svi!2s!4v1571649105340!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen=''></iframe>
" onclick="ShowMap('4','XƯỞNG SẢN XUẤT - THIẾT BỊ NHÀ BẾP INOX - TỪ LIÊM ','0912 220 883','0966 882 100','249 ĐƯỜNG XUÂN PHƯƠNG - TỪ LIÊM - HÀ NỘI (Đối Diện Đình Làng Hậu Ái, Mặt Đường QL70)');"><i class="fa fa-home icon-2"></i>XƯỞNG SẢN XUẤT - THIẾT BỊ NHÀ BẾP INOX - TỪ LIÊM <i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <h3>Showroom Bếp Tốt khu vực Hồ Chí Minh</h3>

                                <button type="button" class="address-link" data-value="1" id="map26" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7336494892857!2d106.62983921483648!3d10.754999062529668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752e822b7c04ab%3A0xf9795e6eaa86bcff!2zMTI2IMSQ4bq3bmcgTmd1ecOqbiBD4bqpbiwgUGjGsOG7nW5nIDEzLCBRdeG6rW4gNiwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1638864548559!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('26','BEPTOT.VN - 126 ĐẶNG NGUYÊN CẨN - PHƯỜNG 13 - QUẬN 6 - HỒ CHÍ MINH','0913 162 612','0969 800 633','126 ĐẶNG NGUYÊN CẨN - PHƯỜNG 13 - QUẬN 6 - HỒ CHÍ MINH');"><i class="fa fa-home icon-2"></i>126 ĐẶNG NGUYÊN CẨN - PHƯỜNG 13 - QUẬN 6 - HỒ CHÍ MINH<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                                <h3>Showroom Bếp Tốt khu vực Thái Nguyên</h3>

                                <button type="button" class="address-link" data-value="1" id="map12" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d551.488491234538!2d105.82276608279291!3d21.587579539377916!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313527218e90cb95%3A0x2fe5903a370daece!2zVmnhu4d0IELhuq9jLCBUcC4gVGjDoWkgTmd1ecOqbiwgVGjDoWkgTmd1ecOqbiwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1592035118229!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('12','BEPTOT.VN - 74 VIỆT BẮC - QUANG TRUNG - THÁI NGUYÊN','0364 322 322','024 35 122 122','SỐ NHÀ 74 KHU DÂN CƯ MỚI VIỆT BẮC');"><i class="fa fa-home icon-2"></i>74 VIỆT BẮC - QUANG TRUNG - THÁI NGUYÊN<i class="fa fa-arrow-circle-o-right icon-1"></i></button>

                            </div>
                            <div class="col-md-4 footer-new-center address-content">

                                <h3 id="ShowroomName">BEPTOT.VN - 467 HOÀNG QUỐC VIỆT - CẦU GIẤY - HÀ NỘI</h3>
                                <p id="ShowroomAddress">467 HOÀNG QUỐC VIỆT - CẦU GIẤY - HÀ NỘI (Có Chỗ Để Xe Ô Tô)</p>
                                <p>Điện thoại: <strong><a href="tel:0986 083 083" id="ShowroomPhone1">0986 083 083 </a></strong>- Hotline: <strong><a href="tel:024 35 122 122" id="ShowroomPhone2">024 35 122 122</a></strong></p>
                                <hr style="width: 100%; background: #fff">
                                <div id="mapnopop">
                                    <img class="lazy" data-src="/Data/upload/images/HQV.png" />
                                </div>


                                <script type="text/javascript">
                                    function ShowMap(mapID, name, phone1, phone2, address) {
                                        var map = $("#map" + mapID).data("map");
                                        $("#ShowroomName").text(name);
                                        $("#ShowroomPhone1").text(phone1);
                                        $("#ShowroomPhone2").text(phone2);
                                        $("#ShowroomAddress").text(address);
                                        $("#mapnopop").html(map);
                                    }
                                </script>

                            </div>
                            <div class="col-md-4 footer-new-left footer-new-right">

                                <h3>Showroom Bếp Tốt khu vực Hải Phòng</h3>

                                <button type="button" class="address-link" id="map24" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2636.752440592603!2d106.68326724917351!3d20.83449008607727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a7a821ac37671%3A0xacbe467d3a585345!2zNDI1IE5ndXnhu4VuIFbEg24gTGluaCwgRMawIEjDoG5nIEvDqm5oLCBMw6ogQ2jDom4sIEjhuqNpIFBow7JuZywgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1616034601608!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('24','BEPTOT.VN - 425 NGUYỄN VĂN LINH - LÊ CHÂN - HẢI PHÒNG','092 326 8888','0985 329 668 ','425 Nguyễn Văn Linh, Dư Hàng Kênh, Lê Chân, Hải Phòng');"><i class="fa fa-home icon-2"></i>425 NGUYỄN VĂN LINH - LÊ CHÂN - HẢI PHÒNG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <button type="button" class="address-link" id="map25" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3728.495488761782!2d106.68270681492959!3d20.852069686094303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a7a8c34d4f827%3A0xcd784a467d6e5f8e!2zMzYgUC4gVMO0IEhp4buHdSwgQ-G6p3UgxJDhuqV0LCBOZ8O0IFF1eeG7gW4sIEjhuqNpIFBow7JuZw!5e0!3m2!1svi!2s!4v1625569391867!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('25','BEPTOT.VN - 36 TÔ HIỆU - NGÔ QUYỀN - HẢI PHÒNG','092 326 8888','0985 329 668 ','36 TÔ HIỆU - NGÔ QUYỀN - HẢI PHÒNG');"><i class="fa fa-home icon-2"></i>36 TÔ HIỆU - NGÔ QUYỀN - HẢI PHÒNG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <button type="button" class="address-link" id="map14" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3726.8220984976983!2d106.67530851540111!3d20.919475896874783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a7b7895b7b3df%3A0x29684b5e285756e4!2zOTEgQuG6oWNoIMSQ4bqxbmcsIFRULiBOw7ppIMSQw6hvLCBUaOG7p3kgTmd1ecOqbiwgSOG6o2kgUGjDsm5nLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1616213702202!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('14','BEPTOT.VN - 91 BẠCH ĐẰNG - THỦY NGUYÊN - HẢI PHÒNG','092 326 8888','0985 329 668 ','91 BẠCH ĐẰNG, THỦY NGUYÊN, HẢI PHÒNG');"><i class="fa fa-home icon-2"></i>91 BẠCH ĐẰNG - THỦY NGUYÊN - HẢI PHÒNG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <button type="button" class="address-link" id="map21" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3728.8268189824985!2d106.68565449682005!3d20.838698594837073!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a7a8360d9030f%3A0x6be0aea5a67a278c!2zODUgxJAuIETDom4gTOG6rXAsIETGsCBIw6BuZyBLw6puaCwgTMOqIENow6JuLCBI4bqjaSBQaMOybmcsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1611631723991!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('21','XƯỞNG SẢN XUẤT TỦ BẾP - LÊ CHÂN - HẢI PHÒNG','0985 329 668','092 326 8888','85 Dân Lập - Dư Hàng Kênh - Lê Chân - Hải Phòng');"><i class="fa fa-home icon-2"></i>XƯỞNG SẢN XUẤT TỦ BẾP - LÊ CHÂN - HẢI PHÒNG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <h3>Showroom Bếp Tốt khu vực Hưng Yên</h3>

                                <button type="button" class="address-link" id="map5" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3728.8234746337353!2d106.00525791424187!3d20.838833599612357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135bb370a1b27a3%3A0x9814c7521ac7be0a!2zU0hPV1JPT00gQkVQVE9ULlZOIEtIT8OBSSBDSMOCVSBIxq9ORyBZw4pO!5e0!3m2!1svi!2s!4v1570617455478!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen=''></iframe>" onclick="ShowMap('5','BEPTOT.VN - QL39A - DÂN TIẾN - KHOÁI CHÂU - HƯNG YÊN','0912 220 883','0984 402 932','QL39A ( SÁT CẦU ĐOÀN VIÊN ) DÂN TIẾN - KHOÁI CHÂU - HƯNG YÊN');"><i class="fa fa-home icon-2"></i>QL39A - DÂN TIẾN - KHOÁI CHÂU - HƯNG YÊN<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <h3>Showroom Bếp Tốt khu vực Hải Dương</h3>

                                <button type="button" class="address-link" id="map19" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4463.571661232417!2d106.30994006933398!3d20.936509458045418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31359b12652b9bc1%3A0x68335f15666611ec!2zNjggxJDhu5cgTmfhu41jIER1LCBQLiBUaGFuaCBUcnVuZywgVGjDoG5oIHBo4buRIEjhuqNpIETGsMahbmcsIEjhuqNpIETGsMahbmcsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1611630901844!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('19','BEPTOT.VN - 68 ĐỖ NGỌC DU - THANH BÌNH - TP HẢI DƯƠNG','0985 329 668','092 326 8888','68 Đỗ Ngọc Du -Thanh Bình - Tp Hải Dương');"><i class="fa fa-home icon-2"></i>68 ĐỖ NGỌC DU - THANH BÌNH - TP HẢI DƯƠNG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <h3>Showroom Bếp Tốt khu vực Bắc Giang</h3>

                                <button type="button" class="address-link" id="map18" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3714.974166583653!2d106.12415634167355!3d21.390921815599658!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313514e487323d4d%3A0x9673bd7c442139be!2zMjggSG_DoG5nIEhvYSBUaMOhbSwgVFQuIENhbyBUaMaw4bujbmcsIFTDom4gWcOqbiwgQuG6r2MgR2lhbmcsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1611630642468!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('18','BEPTOT.VN - 28 HOÀNG HOA THÁM - CẦU GỒ - BẮC GIANG','0985 329 668','092 326 8888','28 Đường Hoàng Hoa Thám - Cầu Gồ - Bắc Giang');"><i class="fa fa-home icon-2"></i>28 HOÀNG HOA THÁM - CẦU GỒ - BẮC GIANG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <button type="button" class="address-link" id="map27" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3717.780054066311!2d106.21247581533481!3d21.28017218452292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31356da48ec3915b%3A0x530643d241f26b5a!2zODY0IEzDqiBM4bujaSwgRMSpbmggS-G6vywgQuG6r2MgR2lhbmcsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1636272662207!5m2!1svi!2s' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe>" onclick="ShowMap('27','BEPTOT.VN - 864 LÊ LỢI - DĨNH KẾ - BẮC GIANG',' 024 35 122 122','0364 322 322','864 LÊ LỢI - DĨNH KẾ - BẮC GIANG');"><i class="fa fa-home icon-2"></i>864 LÊ LỢI - DĨNH KẾ - BẮC GIANG<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <h3>Showroom Bếp Tốt khu vực Lạng Sơn</h3>

                                <button type="button" class="address-link" id="map17" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3711.853557324937!2d106.34390421484846!3d21.513455276430705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31355caad27294e1%3A0x4583a06549935c1b!2zNTQgxJAuIEJhIMSQw6xuaCwgVFQuIEjhu691IEzFqW5nLCBI4buvdSBMxaluZywgTOG6oW5nIFPGoW4sIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1611629577216!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('17','BEPTOT.VN - 54 ĐƯỜNG BA ĐÌNH - TT.HỮU LŨNG - LẠNG SƠN','0985 329 668','092 326 8888','54 Đường Ba Đình - Hữu Lũng - Lạng Sơn');"><i class="fa fa-home icon-2"></i>54 ĐƯỜNG BA ĐÌNH - TT.HỮU LŨNG - LẠNG SƠN<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                                <button type="button" class="address-link" id="map16" data-map="<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3702.815993841247!2d106.75209751485349!3d21.864632164093234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x36b54e69968bf35b%3A0x57316415dafdea26!2zMzcyIMSQLiBUcuG6p24gxJDEg25nIE5pbmgsIEhvw6BuZyBWxINuIFRo4bulLCBUaMOgbmggcGjhu5EgTOG6oW5nIFPGoW4sIEzhuqFuZyBTxqFuLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1611628859372!5m2!1svi!2s' width='600' height='450' frameborder='0' style='border:0;' allowfullscreen='' aria-hidden='false' tabindex='0'></iframe>" onclick="ShowMap('16','BEPTOT.VN - 372 TRẦN ĐĂNG NINH - THÀNH PHỐ LẠNG SƠN','0985 329 668','092 326 8888','372 Trần Đăng Ninh - Hoàng Văn Thụ - Tp Lạng Sơn');"><i class="fa fa-home icon-2"></i>372 TRẦN ĐĂNG NINH - THÀNH PHỐ LẠNG SƠN<i class="fa fa-arrow-circle-o-left icon-1"></i></button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="footer_bottom">
            <div class="container">
                <div class="row">
                    <div class="block-cpn">
                        <h5>© 2016 - Bản quyền của Công ty TNHH sản xuất và thương mại BEPTOT Việt nam</h5>
                        <p>
                            Địa chỉ trụ sở: 330 Đ. Láng, Nhân Chính, Thanh Xuân, Hà Nội
                        </p>
                    </div>
                    <div class="block" style="float: left;">
                        <a href="http://online.gov.vn/Home/WebDetails/29602" rel="noreferrer" aria-label="">
                            <img src="/Content/desktop/images/logo-noikhonghanggia.png" width="36" alt="">
                            <img src="/Content/desktop/images/dathongbao.png" width="120" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>

    </footer>
</div>
<div class="back-to-top">
    <a href="javascript:;">
        <span class="fa fa-angle-up fa-2x"></span>
    </a>
</div>

<script type="text/javascript" src="/Content/desktop/js/jquery.min.js?v=637741327701823527"></script>
<script type="text/javascript" src="/Content/desktop/js/bootstrap.min.js?v=637741327701042548"></script>
<script type="text/javascript" src="/Content/desktop/js/owl.carousel.min.js?v=637741327702448618"></script>
<script type="text/javascript" src="/Content/desktop/js/home.ajax.js?v=637746664537278883"></script>
<script type="text/javascript" src="/Content/desktop/js/product.ajax.js?v=637746616041324245"></script>
<script type="text/javascript" src="/Content/desktop/js/library.js?v=637748404687986618"></script>



</body>
</html>
<!--Copyright © by ONENET.,JSC - https://1net.vn/-->
